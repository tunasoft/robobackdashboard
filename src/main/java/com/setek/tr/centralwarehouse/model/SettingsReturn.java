package com.setek.tr.centralwarehouse.model;

import java.util.List;

public class SettingsReturn {
	private boolean flag;
	private List<String> errors;
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public  List<String> getErrors() {
		return errors;
	}
	public void setErrors( List<String> errors) {
		this.errors = errors;
	}
}
