package com.setek.tr.centralwarehouse.model;


public class FilePojo2{
	
	private String fileName;
	private String fileContent;
	private String fileLocation;
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public FilePojo2(String fileName, String fileContent, String fileLocation) {
		this.fileName = fileName;
		this.fileContent = fileContent;
		this.fileLocation=fileLocation;
	}
	@Override
	public String toString() {
		return "FilePojo [fileName=" + fileName + ", fileContent=" + fileContent + "]";
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileContent() {
		return fileContent;
	}
	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}
}
