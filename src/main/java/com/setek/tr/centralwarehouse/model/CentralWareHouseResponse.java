package com.setek.tr.centralwarehouse.model;

import java.util.Date;

public class CentralWareHouseResponse {

	private boolean success;
	private String message;
	private Date requestDate;
	private final String appName = "Central Warehouse Dashboard";

	public CentralWareHouseResponse() {

	}

	public CentralWareHouseResponse(boolean success, String message, Date requestDate) {
		this.success = success;
		this.message = message;
		this.requestDate = requestDate;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getAppName() {
		return appName;
	}

}
