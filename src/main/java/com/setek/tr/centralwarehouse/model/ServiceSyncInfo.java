package com.setek.tr.centralwarehouse.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.ServiceSyncInfoTbl;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

@Entity
@EqualsAndHashCode(callSuper = false)
@Data
@Table(name = DataBaseConstant.Tables.SERVICE_SYNC_INFO,schema = DataBaseConstant.Schemas.DBO)
public class ServiceSyncInfo extends ServiceSyncInfoTbl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = ID)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column(name = SERVICE_NAME, columnDefinition = "VARCHAR(255)", nullable = false)
	private String serviceName;

	@Column(name = START_DATE)
	private Date startDate;
	
	@Column(name = END_DATE)
	private Date endDate;

	@Column(name = RUNNING, nullable = false, columnDefinition = "tinyint default 0")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean running;

}
