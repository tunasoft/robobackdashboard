package com.setek.tr.centralwarehouse.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StockExcelReportDTO {

    private String articleCode;

    private int ekolStock;

    private int realEkolStock;

    private int dwhStock;

    private int diff;

    private int stockInPlanned;

    private int stockOutPlanned;

    private int ekolExpectedStock;

    private Date controlDate;

    private int warehouseEight;

    private String articleCategory;
}
