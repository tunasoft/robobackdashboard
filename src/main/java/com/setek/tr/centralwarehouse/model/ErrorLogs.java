/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.ErrorLogsTbl;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ASUS
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = DataBaseConstant.Tables.ERROR_LOGS,schema = DataBaseConstant.Schemas.DBO)
public class ErrorLogs extends ErrorLogsTbl implements Serializable {

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    
    @Column(name= METHOD_NAME)
    private String methodName;

    @Column(name = SERVICE_NAME, columnDefinition = "VARCHAR(255)", nullable = false)
    private String serviceName;

	@Column(name = ERROR_DATE)
    private Date errorDate;
    
    @Column(name = SUMMARY_ERROR, columnDefinition = "VARCHAR(500)", nullable = false)
    private String summaryError;
      
    @Column(name = ERROR, columnDefinition = "VARCHAR(8000)", nullable = false)
    private String error;


    
}
