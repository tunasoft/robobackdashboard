package com.setek.tr.centralwarehouse.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ResultOfRequestRobo {

    private boolean isRequestSuccess;
    private String transactionCode;
    private String requestMessages;
    private Date requestDate;


    public void setRequestMessages(String requestMessages) {
        this.requestMessages = requestMessages;
    }

    public void setRequestMessages(List<String> requestMessages) {
        this.requestMessages = "";
        for (String str : requestMessages) {
            this.requestMessages += str + "\n";
        }
    }

}
