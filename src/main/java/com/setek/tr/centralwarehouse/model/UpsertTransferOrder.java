package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.UpsertTransferOrderTbl;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = DataBaseConstant.Tables.UPSERT_TRANSFER_ORDER, schema = DataBaseConstant.Schemas.DBO)
public class UpsertTransferOrder extends UpsertTransferOrderTbl {

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = SENDER_MESSAGE_ID)
    private String senderMessageId;

    @Column(name = TO_OUTLET_ID)
    private int toOutletId;

    @Column(name = TRANSFER_ACTION)
    private String transferAction;

    @Column(name = TRANSFER_REQUEST)
    private String transferRequest;

    @Column(name = TRANSFER_SUB_TYPE)
    private String transferSubType;

    @Column(name = GOODS_SENDER_OUTLET_ID)
    private int goodsSenderOutletId;

    @Column(name = GOODS_RECEIVER_OUTLET_ID)
    private int goodsReceiverOutletId;

    @Column(name = EXTERNAL_ORDER_ID)
    private long externalOrderId;

    @Column(name = SOURCE_SYSTEM)
    private String sourceSystem;

    @Column(name = SOURCE_SYSTEM_ORDER_ID)
    private long sourceSystemOrderId;

    @Column(name = FROM_STOCK_AREA_NAME_ID)
    private int fromStockAreaNameID;

    @Column(name = DELIVERY_DATE)
    private String deliveryDate;

    @Column(name = BOOKING_DATE)
    private String bookingDate;

    @Column(name = POSITION_ID)
    private int positionId;

    @Column(name = PRODUCT_ID)
    private int productId;

    @Column(name = QUANTITY)
    private int quantity;

    @Column(name = STATUS)
    private int status;

    @Column(name = DELIVERY_SIGN)
    private String deliverySign;

    @Column(name = IS_VALID)
    private int isValid;

    @Column(name = IS_SUCCESS)
    private int isSuccess;

    @Column(name = INSERTED_DATE)
    private Date insertedDate;

    @Column(name = CONTROL_COUNT)
    private Integer controlCount = 0;

    @Column(name = PROCESS_DATE)
    private Date processDate;

    @Column(name = PRODUCT_ABT_NO, columnDefinition = "integer default 0")
    private Integer productAbtNo;

    @Column(name = PRODUCT_GROUP_NAME)
    private String productGroupName;

    @Column(name = ART_STOCK, columnDefinition = "integer default 0")
    private Integer artStock;

    @Column(name = SUPPLIER_NO, columnDefinition = "integer default 0")
    private Integer supplierNo;

    @Column(name = SUPPLIER_NAME)
    private String supplierName;
}
