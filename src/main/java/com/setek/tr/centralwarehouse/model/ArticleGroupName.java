package com.setek.tr.centralwarehouse.model;

import lombok.Data;

import javax.persistence.*;

@Table(name = "article_group_name")
@Entity
@Data
public class ArticleGroupName {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, name = "group_code")
    private Integer groupCode;

    @Column(name = "group_name")
    private String group;

    @Column(name = "group_type")
    private String groupType;
}
