/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.RobobackStatusTbl;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *
 * @author ASUS
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = DataBaseConstant.Tables.ROBOBACK_STATUS, schema = DataBaseConstant.Schemas.DBO)
public class RobobackStatus extends RobobackStatusTbl implements Serializable {

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name =  STATUS_NAME, columnDefinition = "VARCHAR(255)", nullable = false)
    private String statuName;

    @Column(name = COLOR_CODE, columnDefinition = "VARCHAR(255)", nullable = false)
    private String colorCode;

    @Column(name = STATUS_CODE, unique = true)
    private int statuCode;

    @Column(name = STATUS_TYPE, columnDefinition = "VARCHAR(255)", nullable = false)
    private String statusType;


}
