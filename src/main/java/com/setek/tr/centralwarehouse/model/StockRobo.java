/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.StockRoboTbl;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ASUS
 */
@Entity
@Getter
@Setter
@Table(name = DataBaseConstant.Tables.STOCK_ROBO, schema = DataBaseConstant.Schemas.DBO)
public class StockRobo extends StockRoboTbl implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = ARTICLE_CODE)
    private String articleCode;

    @Column(name = EKOL_STOCK)
    private int ekolStock;

    @Column(name = DWH_STOCK)
    private Integer dwhStock;

    @Column(name = CONTROL_DATE)
    private Date controlDate;

    @Column(name = STATUS, columnDefinition = "INT default '0'")
    private Integer status;

    @Column(name = EKOL_STOCK_IN_PLANNED)
    private Integer stockInPlanned;

    @Column(name = EKOL_STOCK_OUT_PLANNED)
    private Integer stockOutPlanned;

    @Column(name = EKOL_EXPECTED_STOCK)
    private Integer ekolExpectedStock;

    @Column(name = MATCHED)
    private Integer matched;

}
