/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.TransferTbl;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ASUS
 */
@Entity
@Table(name = DataBaseConstant.Tables.TRANSFER, schema = DataBaseConstant.Schemas.DBO)
@Getter
@Setter
public class Transfer implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = TransferTbl.ID)
    private Long transferId;

    @Column(name = TransferTbl.ORDER_TYPE, columnDefinition = "VARCHAR(40)")
    private String siparisTipi;

    @Column(name = TransferTbl.ORDER_NO, nullable = false)
    private String siparisNo;

    @Column(name = TransferTbl.RETRIEVE_TRANSFER_NO)
    private String aliciTransferNo;

    @Column(name = TransferTbl.ORDER_STATUS, columnDefinition = "VARCHAR(40)")
    private String siparisDurum;

    @Column(name = TransferTbl.LAST_CHANGER, columnDefinition = "VARCHAR(40)")
    private String sonDegistiren;

    @Column(name = TransferTbl.CREATE_DATE)
    private Date olusturmaTarihi;

    @Column(name = TransferTbl.SUPPLIER_NO)
    private String saticiNo;

    @Column(name = TransferTbl.DELIVERY_NO)
    private String irsaliyeNo;

    @Column(name = TransferTbl.SUPPLIER_NAME, columnDefinition = "VARCHAR(40)")
    private String saticiAdi;

    @Column(name = TransferTbl.SHIPMENT_DATE)
    private Date sevkTarihi;

    @Column(name = TransferTbl.STATUS, columnDefinition = "INT default '0'")
    private Status status;

    @Column(name = TransferTbl.ART_NO)
    private int artNo;

    @Column(name = TransferTbl.EKOL_STATUS, columnDefinition = "INT default '0'")
    private Integer ekolStatus;

    @Column(name = TransferTbl.ART_STATUS, columnDefinition = "VARCHAR(1)", nullable = false)
    private String artDurum;

    @Column(name = TransferTbl.ORDER_QUANTITY, nullable = false)
    private int siparisAdet;

    @Column(name = TransferTbl.ENTERED_QUANTITY_BY_STORE, nullable = false)
    private int siparisGirilen;

    @Column(name = TransferTbl.ENTERED_QUANTITY_BY_EKOL, nullable = false)
    private int ekolGirilen;

    @Column(name = TransferTbl.QUANTITY_TRANSFER, nullable = false)
    public int siparisTransfer;

    @Column(name = TransferTbl.PROCESS_DATE)
    private Date processDate;

    @Column(name = TransferTbl.ORDER_TYPE_CODE)
    private Integer orderTypeCode;

    @Column(name = TransferTbl.EDITED_QUANTITY, columnDefinition = "INT default '0'")
    private Integer editedQuantity;

    @Column(name = TransferTbl.EXTERNAL_ORDER_NO, columnDefinition = "VARCHAR(20)")
    private String externalOrderNo;

    @Column(name = "store_code", nullable = true)
    private String storeCode;

    @Column(name = "outlet_id", nullable = true)
    private String outletId;


}
