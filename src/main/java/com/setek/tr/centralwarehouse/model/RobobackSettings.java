/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.commonTools.ServiceNameConverter;
import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.RobobackClientSettingsTbl;
import com.setek.tr.centralwarehouse.constant.ServiceName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author turgut.simsek
 */
@EqualsAndHashCode(callSuper = false)
@Entity
@Data
@Table(name = DataBaseConstant.Tables.ROBOBACK_CLIENT_SETTINGS, schema = DataBaseConstant.Schemas.DBO)
public class RobobackSettings extends RobobackClientSettingsTbl implements Serializable {

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = SERVICE_NAME, columnDefinition = "VARCHAR(255)", nullable = false)
    @Convert(converter = ServiceNameConverter.class)
    private ServiceName serviceName;

    @Column(name = ACTIVE , nullable = false, columnDefinition = "tinyint default 0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean active;

    @Column(name = WORK_PERIODICALLY, nullable = false, columnDefinition = "tinyint default 0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean periodically;

    @Column(name = WORK_HOURLY, nullable = false, columnDefinition = "tinyint default 0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean hourly;

    @Column(name = WORK_DAILY, nullable = false, columnDefinition = "tinyint default 0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean daily;

    @Column(name = WORKING_HOUR)
    private byte hour;

    @Column(name = WORKING_MINUTE)
    private short minute;

    public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	@Column(name = WORKING_HOURS, columnDefinition = "VARCHAR(255)", nullable = false)
    private String workingHours;

    @Column(name = WORK_BY_SPECIFIED_TIME , nullable = false, columnDefinition = "tinyint default 0")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean workBySpecifiedTime;

}
