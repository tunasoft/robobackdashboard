package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.ServiceName;
import com.setek.tr.centralwarehouse.constant.WorkingType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MailSettingDTO {

    private Integer id;

    private List<String> toAddress;

    private List<String> workingTimes;

    private Integer workingMinute;

    private WorkingType workingType;

    private ServiceName serviceName;

    private Boolean active;

}

