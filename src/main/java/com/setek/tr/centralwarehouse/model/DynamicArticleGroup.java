package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = DataBaseConstant.Tables.DYNAMIC_ARTICLE_GROUP)
@Entity
@Getter
@Setter
public class DynamicArticleGroup {

    @Id
    @Column(name = "article_no")
    private Integer articleNo;

    @Column(name = "group_name", columnDefinition = "NVARCHAR(255)")
    private String groupName;

    @Column(name = "group_code", nullable = false)
    private Integer groupCode;

    @Column(name = "mpg")
    private String mpg;

    @Column(name = "productName")
    private String productName;

    @Column(name = "departmentId")
    private Integer departmentId;

    @Column(name = "departmentName")
    private String departmentName;
}

