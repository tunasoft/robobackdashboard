/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.model;

import com.setek.tr.centralwarehouse.constant.DataBaseConstant;
import com.setek.tr.centralwarehouse.constant.TransferOperationLogsTbl;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ASUS
 */
@Getter
@Setter
@Entity
@Table(name = DataBaseConstant.Tables.TRANSFER_OPERATION_LOG, schema = DataBaseConstant.Schemas.DBO)
public class TransferOperationLog extends TransferOperationLogsTbl implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    private Long transferId;

    @Column(name = REQUEST_DATE)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;

    @Column(name = TRANSFER_NO, columnDefinition = "VARCHAR(75)")
    private String transferNo;

    @Column(name = ART_NO, columnDefinition = "INT default '0'")
    private Integer artNo;

    @Column(name = QUANTITY, columnDefinition = "INT default '0'")
    private Integer quantity;

    @Column(name = EDITED_QUANTITY, columnDefinition = "INT default '0'")
    private Integer editedQuantity;

    @Column(name = PROCESS_TYPE, columnDefinition = "VARCHAR(100)")
    private String processType;

    @Column(name = TRANSFER_TYPE, columnDefinition = "VARCHAR(100)")
    private String transferType;

    @Column(name = RQ_TRANSACTION_CODE, columnDefinition = "VARCHAR(1000)")
    private String rqTransactionCode;

    @Column(name = RQ_MESSAGE, columnDefinition = "VARCHAR(500)")
    private String rqMessage;

    @Column(name = RESPONSE_DATE)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date responseDate;

    @Column(name = RESPONSE, columnDefinition = "VARCHAR(100)")
    private String response;

    @Column(name = RP_TRANSACTION_CODE, columnDefinition = "VARCHAR(100)")
    private String rpTransactionCode;

    @Column(name = RP_MESSAGE, columnDefinition = "VARCHAR(500)")
    private String rpMessage;

    @Column(name = USER_NAME, columnDefinition = "VARCHAR(40)")
    private String userName;

}
