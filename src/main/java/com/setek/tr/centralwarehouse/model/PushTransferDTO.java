package com.setek.tr.centralwarehouse.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PushTransferDTO {

    private String fileName = "";

    private String transferAction = "";

    private List<UpsertTransferOrder> upsertTransferOrderList = new ArrayList<>();

    private String message = "";


}
