/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse;

/**
 *
 * @author ASUS
 */
public enum Status {

    New("0", "#F7B857","Yeni Oluşturuldu"),
    SentToEkol("1", "#53CFDB", "Ekole gönderildi"),
    ProcessedbyEKOL("2", "#FFEF30","Sipariş Ekol tarafından işlendi"),
    ProcessedbyRobot("3", "#F0F0E9","Sipariş Robot tarafından işlendi"),
    ProcessedbyRobotFailed("4", "#F47169","Robot işleme  hatalı"),
    ProcessedbyRobotSuccesfully("5", "#B3FC55","Robot işleme başarılı"),
    PartialAcceptanceOfGoodsIn("6", "#F47169","GoodsIn için kısmı kabul"),
    NoReturnbyEKOL("7", "#B3FC55","Ekolden dönüş alınamadı"),
    Cancel("8", "#F47169","İptal"),
    Return("9", "#F47169","Döndü"),
    Closed("10", "#F47169","Kapandı"),
    TestData("11", "#0afcfc","Test verisi"),
    ManualOperation("12", "#F47169","Manuel işlem"),
    CancelRequest("13", "#F47169","İptal isteği"),
    LineDeleted("14", "#F47169","Articel silindi"),
    LineDeletedForStock("15", "#F47169","Artikel Stok için silindi"),
    CancelRequestRejected("16", "#F47169","İptal isteği reddedildi"),
    CancelRequestApproved("17", "#F47169","İptal isteği kabul edildi"),
    PartialAcceptanceOfGoodsOut("18","#F47169" , "Ekol Eksik Mal Çıkışı");


    private final String description;
    private final String color;
    private final String statusName;

     Status(String description, String color, String statusName) {
        this.description = description;
        this.color = color;
        this.statusName = statusName;
    }

    public String getDescription() {
        return description;
    }

    public String getColor() {
        return color;
    }

    public String getStatusName() {
        return statusName;
    }
}
