package com.setek.tr.centralwarehouse;

public enum ServiceInfo {

    ROBOBACK_DB_SYNCHRONIZER("localhost", 8091, "Database Eşitleyici"), ROBOBACK_CLIENT("localhost", 9091, "Roboback Istemci");


    private final String hostName;
    private final int port;
    private final String serviceName;


    ServiceInfo(String hostName, int port, String serviceName) {
        this.hostName = hostName;
        this.port = port;
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getHostName() {
        return hostName;
    }

    public int getPort() {
        return port;
    }
}
