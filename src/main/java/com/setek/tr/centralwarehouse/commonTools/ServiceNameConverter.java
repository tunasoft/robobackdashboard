package com.setek.tr.centralwarehouse.commonTools;

import com.setek.tr.centralwarehouse.constant.ServiceName;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ServiceNameConverter implements AttributeConverter<ServiceName, String> {


    @Override
    public String convertToDatabaseColumn(ServiceName attribute) {
        return attribute.toString();
    }

    @Override
    public ServiceName convertToEntityAttribute(String dbData) {
        return ServiceName.valueOf(dbData);
    }
}
