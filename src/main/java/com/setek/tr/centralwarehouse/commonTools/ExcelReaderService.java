/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.commonTools;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ExcelReaderService {

    private StringBuilder header;
    private List<String> rows;
    private File file;
    public static final String SPLIT_PARAMETER = "$";

    private final Logger log = LoggerFactory.getLogger(ExcelReaderService.class);

    public ExcelReaderService(File file) throws EncryptedDocumentException, IOException {
        this.file = file;
        rows = new ArrayList<>();
        readExcel();
    }

    private void readExcel() throws EncryptedDocumentException, IOException {

        FileInputStream fis = new FileInputStream(file);
        Workbook workbook;
        // Creating a Workbook from an Excel file (.xls or .xlsx)
        if (file.getName().contains("xlsx")){
           workbook  =  new XSSFWorkbook(fis);
        }else{
            workbook = new HSSFWorkbook(fis);
        }


        // Retrieving the number of sheets in the Workbook
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        boolean headerFlag = true;

        header = new StringBuilder();
        StringBuilder editrow = new StringBuilder();

        for (Row row : sheet) {
            for (Cell cell : row) {
                String cellValue = dataFormatter.formatCellValue(cell);

                if (cellValue.isEmpty()) {
                    cellValue = "0";
                }

                if (headerFlag) {
                    header.append(cellValue).append(SPLIT_PARAMETER);
                } else {
                    editrow.append(cellValue).append(SPLIT_PARAMETER);
                }

            }

            if (headerFlag) {
                header.append(header.substring(0, header.length() - 1));
                headerFlag = false;
            } else {

                if (!editrow.toString().isEmpty()){
                rows.add(editrow.toString());
                }editrow = new StringBuilder();
            }
        }

        try {
           // Closing the workbook
            workbook.close();
        }catch (Exception e){
          log.error("ExcelReaderService Getrows -> " ,e);
        }

    }

    public String getHeader() {
        return header.toString();
    }

    public List<String> getRows() {
        return rows;
    }

}
