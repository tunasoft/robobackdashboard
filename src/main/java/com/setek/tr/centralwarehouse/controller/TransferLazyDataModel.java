package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.RobobackStatus;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.services.TransferDataService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public class TransferLazyDataModel extends LazyDataModel<Transfer> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private TransferDataService transferService;

    private Date startDate;

    private Date endDate;

    private List<RobobackStatus> ekolStatusList;

    Map<String, Object> filters2;

    private Map<String, String> filterProps;

    //    public TransferLazyDataModel(TransferDataService transferService, Date startDate, Date endDate) {
//        log.info("TransferLazyDataModel.TransferLazyDataModel()");
//        this.transferService = transferService;
//        this.startDate = startDate;
//        this.endDate = endDate;
//    }
    public TransferLazyDataModel(TransferDataService transferService, Date startDate, Date endDate, List<RobobackStatus> ekolStatusList, Map<String, String> filterProps) {
        log.info("TransferLazyDataModel.TransferLazyDataModel()");
        this.transferService = transferService;
        this.startDate = startDate;
        this.endDate = endDate;
        this.ekolStatusList = ekolStatusList;
        this.filterProps = filterProps;
    }


    //    public Map<String, Object> getFilters2() {
//        return filters2;
//    }
//
//    public void setFilters2(Map<String, Object> filters2) {
//        this.filters2 = filters2;
//    }
    private Logger log = LoggerFactory.getLogger(TransferLazyDataModel.class);

    //    private List<Transfer> transferListforExcel;
    public List<Transfer> getTrasnferDataForExcel() {

        List<Transfer> transferList = new ArrayList<>();

        if (filters2 != null && !filters2.keySet().isEmpty()) {
            Map<String, Object> paramMaps = convertToSqlParam(filters2);
            Map<String, String> filterOptions = convertFilterPropsByColumnName(filterProps);
            transferList = transferService.retrieveTransfersByParamforExcel(paramMaps, filterOptions);
        } else {
            transferList = transferService.getTransfersTimeBetween(startDate, endDate);
        }

        return transferList.stream().filter(t -> t.getOrderTypeCode() != 8).collect(Collectors.toList());
    }

    @Override
    public List<Transfer> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        filters2 = filters;
        log.info("TransferLazyDataModel.load()");

        int totalDataSize = 0;

        log.info("TransferLazyDataModel.load() -> {}", (filters != null && !filters.isEmpty()));

        List<Transfer> transferData = new ArrayList<>();


        if (filters != null && !filters.isEmpty()) {

            Map<String, Object> paramMaps = convertToSqlParam(filters);

            Map<String, String> filterProps = convertFilterPropsByColumnName(this.filterProps);
//            List<Transfer> filteredList = new ArrayList<Transfer>();

            transferData = transferService.retrieveTransfersByParam(paramMaps, first, pageSize, sortField, sortOrder, filterProps);
//
            totalDataSize = transferService.retrieveTransfersCountByParam(paramMaps, filterProps).intValue();
            this.setRowCount(totalDataSize);

            return transferData;

        }

        transferData = transferService.getTransfersTimeBetween(startDate, endDate, first, pageSize, sortField, sortOrder);

        totalDataSize = transferService.getTransfersTimeBetweenCount(startDate, endDate).intValue();

        this.setRowCount(totalDataSize);

        return transferData;

    }

    private Map<String, Object> convertToSqlParam(Map<String, Object> filters) {
        Map<String, Object> paramMaps = new LinkedHashMap<>();
        for (String key : filters.keySet()) {

            Object value = filters.get(key);

            key = key.equals("siparisTransfer")? "quantity_transfer":key;
            key = key.equals("processDate") ? "process_date" : key;
            key = key.equals("orderTypeCode") ? "order_type_code" : key;
            key = key.equals("ekolStatus") ? "ekol_status" : key;
            key = key.equals("artNo") ? "art_no" : key;
            key = key.equals("siparisAdet") ? "order_quantity" : key;
            key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
            key = key.equals("olusturmaTarihi") ? "create_date" : key;
            key = key.equals("sevkTarihi") ? "shipment_date" : key;
            key = key.equals("artDurum") ? "art_status" : key;
            key = key.equals("ekolGirilen") ? "entered_quantity_by_ekol":key;
            if (key.equals("status")) {
                value = Status.valueOf(value.toString()).getDescription();

            } else if (key.equals("ekol_status")) {
                value = getStatuCode(value.toString());
            }

            if (value instanceof String) {
                value = ((String) value).trim();
            }

            paramMaps.put(key, value);

        }
        return paramMaps;
    }

    private Map<String, String> convertFilterPropsByColumnName(Map<String, String> filterProps) {
        Map<String, String> convertedMap = new HashMap<>();

        for (Map.Entry<String, String> entry : filterProps.entrySet()) {
            String key = entry.getKey();

            switch (key) {
                case "siparisAdet":
                    key = "order_quantity";
                    break;
                case "siparisGirilen":
                    key = "entered_quantity_by_store";
                    break;

                case "ekolGirilen":
                    key = "entered_quantity_by_ekol";
                    break;
                case "siparisTransfer":
                    key = "quantity_transfer";
                    break;
                default:
                    log.info("ConvertFilterProp Key Not Found -> {}", key);
                    break;
            }
            convertedMap.put(key, entry.getValue());
        }


        return convertedMap;
    }

    public Integer getStatuCode(String statuName) {

        for (RobobackStatus robobackStatus : ekolStatusList) {
            if (robobackStatus.getStatuName().equals(statuName)) {
                return robobackStatus.getStatuCode();
            }
        }
        return 0;
    }

}
