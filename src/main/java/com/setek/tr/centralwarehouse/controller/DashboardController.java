package com.setek.tr.centralwarehouse.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import com.setek.tr.centralwarehouse.ServiceInfo;
import com.setek.tr.centralwarehouse.services.*;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.model.UpsertTransferOrder;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ViewScoped
@Controller
public class DashboardController implements Serializable {

    private final Logger log = LoggerFactory.getLogger(DashboardController.class);



    @Autowired
    private ExcelService excelService;

    @Autowired
    private TransferDataService transferDataService;

    @Autowired
    private ErrorLogsService errorLogsService;

    @Autowired
    private UpsertDataService upserDataService;

    private OpenOrderLazyDataModel openOrderLazyDataModel;

    @Autowired
    private GoodsInTransferLazyModel goodsInTransferLazyModel;
   
    @Autowired
    private GoodsOutTransferLazyModel goodsOutTransferLazyModel;
    
    private Date goodsInCalendar;
    private Date inventDate;
    private LogsLazyModel logsLazyModel;
    
    @Autowired
    private TransferLazyModelForRobotFailed robotFailedModel;

    private int upsertArticleOrder;
    private int upsertStoreCount;
    private int upsertArticleCount;
    private int upsertTotalLine;

    private int transferArticleOrder;
    private int transferStoreCount;
    private int transferArticleCount;
    private int transferTotalLine;

    private List<Transfer> inventTransfers;

    @Autowired
    private CheckSocketAliveService checkSocketAliveService;

    private List<ServiceInfo> serviceInfos;

    @PostConstruct
    public void init() {
        log.info("DashboardController.init()");
        goodsInCalendar = DateUtils.truncate(new Date(), java.util.Calendar.DAY_OF_MONTH);

        inventDate = DateUtils.truncate(new Date(), java.util.Calendar.DAY_OF_MONTH);

        openOrderLazyDataModel = new OpenOrderLazyDataModel(transferDataService);

        logsLazyModel = new LogsLazyModel(errorLogsService);

        serviceInfos = Arrays.asList(ServiceInfo.values());

        this.initInventReport();

    }

    public void initInventReport() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());

        String tomorrow = "";

        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(today));
            cal.add(Calendar.DAY_OF_MONTH, +1);
            tomorrow = sdf.format(cal.getTime());
        } catch (ParseException e) {
            log.error("initInventReport -> " , e);
        }

        List<UpsertTransferOrder> upsertOrders = upserDataService.getTodayLoadedUpserts(today, tomorrow);

        if (upsertOrders != null && !upsertOrders.isEmpty()) {

            extractUpsertOrders(upsertOrders);

        } else {

            FacesMessage message = new FacesMessage("Empty", "Invent file was not uploaded on the specified date.");

            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }

    public void reloadInventReportByDate() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(inventDate);
        String tomorrow = "";

        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(today));
            cal.add(Calendar.DAY_OF_MONTH, +1);
            tomorrow = sdf.format(cal.getTime());
        } catch (ParseException e) {
           log.error("reloadInventReportByDate() -> " , e);
        }

        List<UpsertTransferOrder> upsertOrders = upserDataService.getTodayLoadedUpserts(today, tomorrow);

        if (!upsertOrders.isEmpty()) {

            extractUpsertOrders(upsertOrders);
        } else {
            FacesMessage message = new FacesMessage("Empty", "Invent file was not uploaded on the specified date.");

            FacesContext.getCurrentInstance().addMessage(null, message);

        }
    }

    private void extractUpsertOrders(List<UpsertTransferOrder> upsertOrders) {
        HashSet<String> externalIds = new HashSet<>();

        upsertOrders.forEach(upsertTransferOrder -> externalIds.add(String.valueOf(upsertTransferOrder.getExternalOrderId())));

        inventTransfers = transferDataService.getTransferByExternalOrderId(externalIds);

        /*------------------------------------------------------------------------------------------------------------------*/
        HashSet<Integer> upsertStore = new HashSet<>();
        HashSet<Integer> upsertArticle = new HashSet<>();

        upsertArticleOrder = 0;
        upsertStoreCount = 0;
        upsertArticleCount = 0;
        upsertTotalLine = upsertOrders.size();

        for (UpsertTransferOrder upsertOrder : upsertOrders) {
            upsertStore.add(upsertOrder.getGoodsReceiverOutletId());
            upsertArticle.add(upsertOrder.getProductId());
            upsertArticleOrder += upsertOrder.getQuantity();
        }

        upsertStoreCount = upsertStore.size();
        upsertArticleCount = upsertArticle.size();

        /*------------------------------------------------------------------------------------------------------------------*/
        HashSet<String> transferStore = new HashSet<>();
        HashSet<Integer> transferArticle = new HashSet<>();
        transferArticleOrder = 0;
        transferStoreCount = 0;
        transferArticleCount = 0;
        transferTotalLine = inventTransfers.size();

        for (Transfer transfer : inventTransfers) {

            transferStore.add(transfer.getSiparisNo());
            transferArticle.add(transfer.getArtNo());
            transferArticleOrder += transfer.getSiparisAdet();
        }

        transferArticleCount = transferArticle.size();
        transferStoreCount = transferStore.size();
    }

    public void exportTransfersToExcel(DashboardLazyDataModel lazyDataModel, String headerTypeOfReport, String reportName) {
        Workbook workbook = this.excelService.prepareExcelReportOfTheTransfers(lazyDataModel.getFilteredDataForExcelReport(), headerTypeOfReport, reportName);
        sendFileToBrowser(workbook, reportName);
    }

    public void sendFileToBrowser(Workbook workbook, String reportName) {
        try {
            String filename = reportName + ".xlsx";

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            externalContext.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            // http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(externalContext.getResponseOutputStream()); // get workbook
            facesContext.responseComplete(); // if I don't call responseComplete() => IllegalStateException

        } catch (Exception e) {
           log.error("sendFileToBrowser -> " , e);
        }
    }

    public boolean checkSocketIsAlive(ServiceInfo serviceInfo){
      return  checkSocketAliveService.checkSocketForServiceIsAlive(serviceInfo);
    }


}
