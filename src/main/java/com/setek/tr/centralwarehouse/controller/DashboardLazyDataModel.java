/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.model.Transfer;
import java.util.List;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author ASUS
 */
public abstract class DashboardLazyDataModel extends LazyDataModel<Transfer> {

    public abstract List<Transfer> getFilteredDataForExcelReport();

}
