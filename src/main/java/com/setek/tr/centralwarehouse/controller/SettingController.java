package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.RestClients.RobobackResponse;
import com.setek.tr.centralwarehouse.constant.ServiceName;
import com.setek.tr.centralwarehouse.constant.WorkingType;
import com.setek.tr.centralwarehouse.model.MailSettingDTO;
import com.setek.tr.centralwarehouse.model.RobobackSettings;
import com.setek.tr.centralwarehouse.model.SettingsReturn;
import com.setek.tr.centralwarehouse.services.RobobackSettingService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.*;
import java.util.regex.Pattern;

//@SessionScoped
@Lazy
@Controller
@Getter
@Setter
public class SettingController {

    private final Logger logger = LoggerFactory.getLogger(SettingController.class);

    private RobobackSettingService robobackSettingsService;

    private List<RobobackSettings> robobackSettings;

    private List<MailSettingDTO> mailSettings;

    private List<WorkingType> workingTypes;

    private List<ServiceName> serviceNames;

    private MailSettingDTO selectedMailSetting;

    @Autowired
    public SettingController(RobobackSettingService robobackSettingService) {
        this.robobackSettingsService = robobackSettingService;
    }

    @PostConstruct
    public void init() {
        try {


            robobackSettings = robobackSettingsService.findAll();
            mailSettings = robobackSettingsService.getMailSettings();

            workingTypes = Arrays.asList(WorkingType.values());
            serviceNames = Arrays.asList(ServiceName.values());

        } catch (Exception e) {
            logger.error("Error on Settings init() -> ", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn", e.getMessage()));
        }

    }

    public void saveMailList() {
        FacesContext current = FacesContext.getCurrentInstance();

        try {
            boolean isValid = checkMailSettings(current);

            if (isValid) {
                RobobackResponse response = robobackSettingsService.saveMailSettings(mailSettings);
                if (response.isSuccess()) {
                    current.addMessage(null, new FacesMessage("Success", "Process Successfully completed"));
                } else {
                    List<String> warnMessages = (List<String>) response.getResponseData().getOrDefault("warningMessages", Collections.EMPTY_LIST);

                    if (!warnMessages.isEmpty()) {
                        warnMessages.forEach(message -> current.addMessage(null, new FacesMessage("Warning", message)));
                    } else {
                        current.addMessage(null, new FacesMessage("Warning", "Please check inputs."));
                    }

                }
            }
        } catch (Exception e) {
            logger.error("Error on saveMailList -> ", e);
            current.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Save Mail Settings", e.getMessage()));
        }
    }

    private boolean checkMailSettings(FacesContext context) {
        boolean result = true;

        for (MailSettingDTO mailSetting : mailSettings) {
            boolean checkResult = checkMailSettingByWorkingType(mailSetting, context);
            if (result) {
                result = checkResult;
            }
        }

        return result;
    }

    private boolean checkMailSettingByWorkingType(MailSettingDTO mailSetting, FacesContext context) {

        switch (mailSetting.getWorkingType()) {
            case HOURLY:
            case PERIODICALLY:
                return checkMinute(context, mailSetting);
            case SPECIFIED_TIMES:
                return checkHour(context, mailSetting);
            default:
                context.addMessage(null, new FacesMessage(mailSetting.getWorkingType().getName() + " -> not found"));
                return false;
        }
    }

    private boolean checkHour(FacesContext context, MailSettingDTO mailSetting) {
        if (mailSetting.getWorkingTimes() == null || mailSetting.getWorkingTimes().isEmpty()) {
            context.addMessage(null, new FacesMessage("Working Times could not be empty please check!"));
            return false;
        }

        mailSetting.getWorkingTimes().forEach(time -> validateHourTime(context, time));

        return !context.isValidationFailed();
    }

    private boolean checkMinute(FacesContext context, MailSettingDTO mailSetting) {
        if (mailSetting.getWorkingMinute() == null) {
            context.addMessage("", new FacesMessage("The runtime could not be null. Please check"));
            context.validationFailed();
        }

        int minute = mailSetting.getWorkingMinute();

        if (mailSetting.getWorkingType().equals(WorkingType.HOURLY)) {
            if (minute < 0 || minute > 59) {
                context.addMessage(null, new FacesMessage("Minute not valid. Minute must be between 0 - 59"));
                context.validationFailed();
            }
        } else {
            if (minute < 0) {
                context.addMessage(null, new FacesMessage("Minute not valid. Must be greater than or equal 0"));
                context.validationFailed();
            }
        }
        return !context.isValidationFailed();
    }

    public List<RobobackSettings> getRobobackSettings() {
        return robobackSettings;
    }

    public void setRobobackSettings(List<RobobackSettings> robobackSettings) {
        this.robobackSettings = robobackSettings;
    }

    public void save() {

        SettingsReturn settingsReturn = robobackSettingsService.save(robobackSettings);
        robobackSettings = robobackSettingsService.findAll();
        FacesContext context = FacesContext.getCurrentInstance();
        if (!settingsReturn.isFlag() && settingsReturn.getErrors().isEmpty()) {

            context.addMessage(null, new FacesMessage("Unsuccess", "At least one working type must be selected"));
        } else if (!settingsReturn.isFlag() && !settingsReturn.getErrors().isEmpty()) {

            Set<String> settingsSet = new HashSet<String>(settingsReturn.getErrors());
            for (String error : settingsSet) {

                context.addMessage(null, new FacesMessage("Unsuccess", "Something wrong in Setting Type:" + error));
            }
        } else {

            context.addMessage(null, new FacesMessage("Success", "Settings were edited successfully"));

        }
    }

    public void triggerMailService() {

        FacesContext context = FacesContext.getCurrentInstance();

        if (selectedMailSetting == null) {
            context.validationFailed();
            context.addMessage(null, new FacesMessage("Warn", "Please make a choice"));
            return;
        }

        if (selectedMailSetting.getServiceName() == null) {
            context.validationFailed();
            context.addMessage(null, new FacesMessage("Warn", "Service name undefined"));
        }

        robobackSettingsService.triggerService(context, selectedMailSetting.getServiceName());

    }


    public void triggerService() {

        String serviceName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("serviceName");
        FacesContext context = FacesContext.getCurrentInstance();

        robobackSettingsService.triggerService(context, ServiceName.valueOf(serviceName));

    }

    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        logger.info("EmailListValidator: {}", value);
        if (value == null) {
            context.addMessage(null, new FacesMessage("Value could not be null! please check"));
            context.validationFailed();
            return;
        }

        if (value instanceof List) {
            List<String> emailList = (List<String>) value;
            for (String emailAddress : emailList)
                validateEmail(context, component, StringUtils.normalizeSpace(emailAddress));

        } else if (value instanceof String) {
            String email = (String) value;
            validateEmail(context, component, StringUtils.normalizeSpace(email));
        }


    }

    public void validateWorkingTimes(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        logger.info("workingTime Validator value ->  {}", value);
        if (value == null) {
            logger.debug("validateWorkingTimes skipping validation because email is blank");
            context.addMessage(null, new FacesMessage("Value could not be null! please check"));
            context.validationFailed();
        }

        List<String> timeList = (List<String>) value;
        for (String emailAddress : timeList)
            validateHourTime(context, StringUtils.normalizeSpace(emailAddress));
    }

    private void validateHourTime(FacesContext context, String normalizeSpace) {
        if (normalizeSpace.isEmpty()) {
            context.addMessage(null, new FacesMessage("Warn", "Time empty"));
            context.validationFailed();
        }
        if (!normalizeSpace.contains(":")) {
            context.addMessage(null, new FacesMessage("Warn", "Time format error split  (':') not found in " + normalizeSpace));
            context.validationFailed();
        }

        String[] splittedTime = normalizeSpace.split(":");

        if (splittedTime.length != 2) {
            context.addMessage(null, new FacesMessage("Warn", "Time format error please check ->" + normalizeSpace));
            context.validationFailed();
            return;
        }

        try {
            int hour = Integer.parseInt(splittedTime[0]);
            int minute = Integer.parseInt(splittedTime[1]);
            if (hour < 0 || hour > 23 || minute < 0 || minute > 60) {
                context.addMessage(null, new FacesMessage("Warn", "Hour range 00 - 23 Minute range 00 - 60 please check  -> " + normalizeSpace));
                context.validationFailed();
            }


        } catch (Exception numberFormatException) {
            context.addMessage(null, new FacesMessage("Warn", "Time Format exception cannot cast number -> " + normalizeSpace));
            context.validationFailed();
        }
    }

    private void validateEmail(FacesContext context, UIComponent component, String value) {

        Pattern pattern;

        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);

        if (value == null) {
            context.addMessage(null, new FacesMessage("Warn", "value could not be null!!"));
            context.validationFailed();
            return;
        }

        if (!pattern.matcher(value).matches()) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error",
                    value + " is not a valid email"));
            context.validationFailed();
        }
    }

}
