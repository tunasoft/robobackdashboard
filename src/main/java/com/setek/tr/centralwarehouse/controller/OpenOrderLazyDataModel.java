package com.setek.tr.centralwarehouse.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.services.TransferDataService;

public class OpenOrderLazyDataModel extends DashboardLazyDataModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private TransferDataService transferService;
    private Logger log = LoggerFactory.getLogger(OpenOrderLazyDataModel.class);

    private Map<String, Object> filters;

    public OpenOrderLazyDataModel(TransferDataService transferService) {
        this.transferService = transferService;
    }

    @Override
    public List<Transfer> getFilteredDataForExcelReport() {
        return transferService.retrieveOpenOrdersByParamForExcel(filters);
    }

    @Override
    public List<Transfer> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        this.filters = filters;
      
        log.info("OpenOrderLazyDataModel.load()");

        if (filters != null && !filters.isEmpty()) {

            Map<String, Object> paramMaps = new LinkedHashMap<>();

            for (String key : filters.keySet()) {

                Object value = filters.get(key);

                log.info("load ket : " + key + " value : " + value.toString());

                key = key.equals("artNo") ? "art_No" : key;
                key = key.equals("siparisAdet") ? "order_quantity" : key;
                key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
                key = key.equals("olusturmaTarihi") ? "create_date" : key;
                key = key.equals("sevkTarihi") ? "shipment_date" : key;
                key = key.equals("artDurum") ? "art_status" : key;

                if (key.equals("status")) {
                    value = Status.valueOf(value.toString()).getDescription();

                }

                paramMaps.put(key, value);

            }

            int totalDataSize = transferService.retrieveOpenOrdersByParamCount(paramMaps).intValue();
            this.setRowCount(totalDataSize);

            List<Transfer> list = transferService.retrieveOpenOrdersByParam(first, pageSize, sortField, sortOrder, paramMaps);

            return list;
        }

        int totalDataSize = transferService.retriveOpenOrdersCount().intValue();
        this.setRowCount(totalDataSize);

        return transferService.retrieveOpenOrders(first, pageSize, sortField, sortOrder);

    }

}
