package com.setek.tr.centralwarehouse.controller;

import java.util.*;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.services.TransferDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class   GoodsInTransferLazyModel extends DashboardLazyDataModel {
	private static final long serialVersionUID = 1L;
    private final Logger log = LoggerFactory.getLogger(GoodsInTransferLazyModel.class);

	@Autowired
	private TransferDataService transferService;


	@Getter
	@Setter
	@Autowired
	private Date requestedDate;
	
        private Map<String, Object> filters;
        
/*	public GoodsInTransferLazyModel(TransferDataService transferService, Date requestedDate) {
		log.info("TransferLazyDataModel.TransferLazyDataModel()");
		this.transferService = transferService;
		this.requestedDate=requestedDate;	
	} */
	
	@Override
	public List<Transfer> getFilteredDataForExcelReport(){
		return transferService.retrieveGoodsInPastTransferForFilterForExcel(filters, requestedDate);
	}


	@Override
	public List<Transfer> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		this.filters = filters;
		log.info("TransferLazyDataModel.load()");

		int totalDataSize = 0;

		log.info("TransferLazyDataModel.load() -> " + (filters != null && !filters.isEmpty()));
		
		if (filters != null && !filters.isEmpty()) {

			Map<String, Object> paramMaps = new LinkedHashMap<>();

			for (String key : filters.keySet()) {

				Object value = filters.get(key);

				log.info("load ket : " + key + " value : " + value.toString());

				key = key.equals("artNo") ? "art_no" : key;
				key = key.equals("siparisAdet") ? "order_quantity" : key;
				key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
				key = key.equals("olusturmaTarihi") ? "create_date" : key;
				key = key.equals("sevkTarihi") ? "shipment_date" : key;
				key = key.equals("artDurum") ? "art_status" : key;

				if (key.equals("status")) {
					value =  Status.valueOf(value.toString()).getDescription();

				}
				
				
				paramMaps.put(key, value);
				

			}
 
			List<Transfer> filteredList = new ArrayList<Transfer>();

			filteredList = transferService.retrieveGoodsInPastTransferForFilter(paramMaps, first, pageSize, sortField, sortOrder,requestedDate);
//
			totalDataSize = transferService.retrieveGoodsInPastTransferCountForFilter(paramMaps, requestedDate).intValue();
			this.setRowCount(totalDataSize);

			return filteredList;

		}


			List<Transfer> transfers= transferService.retrieveGoodsInPastTransfer(first, pageSize, sortField, sortOrder,requestedDate);

			totalDataSize = transferService.retrieveGoodsInPastCount(requestedDate).intValue();
			this.setRowCount(totalDataSize);

			return transfers;

		}

	public void refreshGoodsInTransferLazyModel() {
		load(0, 0, null, SortOrder.DESCENDING, new HashMap<>());
	}

}
