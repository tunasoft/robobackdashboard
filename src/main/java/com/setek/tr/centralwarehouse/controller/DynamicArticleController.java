package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.model.DynamicArticleGroup;
import com.setek.tr.centralwarehouse.services.DynamicArticleGroupService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.text.ParseException;

@Getter
@Setter
@ViewScoped
@Controller
public class DynamicArticleController {

    private final Logger log = LoggerFactory.getLogger(DynamicArticleController.class);

    private DynamicArticleGroupLazyDataModel dynamicArticleGroupsLDM;

    @Autowired
    private DynamicArticleGroupService dynamicArticleGroupService;


    @PostConstruct
    public void init() {

        log.info("dynamic article group initialize");
        dynamicArticleGroupsLDM = new DynamicArticleGroupLazyDataModel(dynamicArticleGroupService);

        log.info("build test");
    }


    public void filterIt() throws ParseException {
        log.info("DynamicArticleController.filterIt() start");
        dynamicArticleGroupsLDM = new DynamicArticleGroupLazyDataModel(dynamicArticleGroupService);
        log.info("DynamicArticleController.filterIt() end");
    }


    public void onRowEdit(RowEditEvent rowEditEvent) {
        dynamicArticleGroupService.saveArticle((DynamicArticleGroup) rowEditEvent.getObject());
        FacesMessage msg = new FacesMessage("Successfully edited", String.valueOf(((DynamicArticleGroup) rowEditEvent.getObject()).getArticleNo()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(((DynamicArticleGroup) event.getObject()).getArticleNo()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
