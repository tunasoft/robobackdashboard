package com.setek.tr.centralwarehouse.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.setek.tr.centralwarehouse.model.StockRobo;
import com.setek.tr.centralwarehouse.services.StockService;

public class StockLazyDataModel extends LazyDataModel<StockRobo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StockService stockService;
	private Date startDate;
	private Date endDate;
	private Logger log = LoggerFactory.getLogger(StockLazyDataModel.class);
	private Map<String, Object> filters2;

	public Map<String, Object> getFilters2() {
		return filters2;
	}

	public void setFilters2(Map<String, Object> filters2) {
		this.filters2 = filters2;
	}

	public StockLazyDataModel(StockService stockService, Date startDate, Date endDate) {
		log.info("StockLazyDataModel.StockLazyDataModel()");
		this.stockService = stockService;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public List<StockRobo> load2(Map<String, Object> filters) {

		if (filters.keySet().size() != 0) {
			
			return stockService.retrieveStocksByParamForExcel(filters);
		}

		else
			return stockService.getStocksTimeBetween(startDate, endDate);
	}

	@Override
	public List<StockRobo> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		filters2 = filters;
		log.info("StockLazyDataModel.load()");
		System.out.println("StockLazyDataModel.load() first = " + first + "  pagesize = " + pageSize + " sortfield = "
				+ sortField + " filters " + filters);
		System.out.println(
				"StockLazyDataModel.load() sartdate  " + new SimpleDateFormat("yyyy-MM-dd hh:mm").format(startDate)
						+ "   end date " + new SimpleDateFormat("yyyy-MM-dd hh:mm").format(endDate));
		int totalDataSize = 0;

		if (filters != null && !filters.isEmpty()) {

			System.out.println("StockLazyDataModel.load() filters is not empty");

			Map<String, Object> paramMaps = new LinkedHashMap<>();

			for (String key : filters.keySet()) {

				Object value = filters.get(key);

				System.out.println("StockLazyDataModel.load() filters key " + key + " value = " + value);

				key = key.equals("ekolStock") ? "ekol_Stock" : key;
				key = key.equals("dwhStock") ? "dwh_Stock" : key;
				key = key.equals("controlDate") ? "control_Date" : key;
				
				if (value instanceof String) {
					value = ((String) value).trim();
				}
				
				paramMaps.put(key, value);

			}
			

			List<StockRobo> filteredList = new ArrayList<StockRobo>();

			filteredList = stockService.retrieveStocksByParam(paramMaps, first, pageSize, sortField, sortOrder);
//
			totalDataSize = stockService.retrieveStocksCountByParam(paramMaps).intValue();
			this.setRowCount(totalDataSize);

			return filteredList;

		}

		List<StockRobo> stocks = stockService.getStocksTimeBetween(startDate, endDate, first, pageSize, sortField,
				sortOrder);

		totalDataSize = stockService.getStocksTimeBetweenCount(startDate, endDate).intValue();

		this.setRowCount(totalDataSize);

		return stocks;

	}

	public StockLazyDataModel() {
		super();
	}

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
