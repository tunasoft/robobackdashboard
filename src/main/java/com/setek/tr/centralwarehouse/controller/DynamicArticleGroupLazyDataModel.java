package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.model.DynamicArticleGroup;
import com.setek.tr.centralwarehouse.services.DynamicArticleGroupService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class DynamicArticleGroupLazyDataModel extends LazyDataModel<DynamicArticleGroup> {

    private Logger log = LoggerFactory.getLogger(DynamicArticleGroupLazyDataModel.class);

    private final DynamicArticleGroupService dynamicArticleGroupService;

    public DynamicArticleGroupLazyDataModel(DynamicArticleGroupService dynamicArticleGroupService) {
        this.dynamicArticleGroupService = dynamicArticleGroupService;
    }

    @Override
    public List<DynamicArticleGroup> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        log.info("DynamicArticleGroupLazyDataModel.load()");

        this.setRowCount(dynamicArticleGroupService.retrieveTotalDataCount().intValue());

        return dynamicArticleGroupService.retrieveTransfersByParam(filters, first, pageSize, sortField, sortOrder);
    }


}
