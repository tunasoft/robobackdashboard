package com.setek.tr.centralwarehouse.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
public class LogControllerService {

	private String logs;
	private String logs2;
	private String temp;
	
    public String getLogs2() {
		return logs2;
	}

	public void setLogs2(String logs2) {
		this.logs2 = logs2;
	}

	public String getLogs() {
		return logs;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}
	
	public void change() {
		if(!temp.equals(logs2)) {
	    this.logs2=logs;
		}
	}

	@RequestMapping("/logs")
    public ResponseEntity<String> logs(@RequestParam String logs2) {
    	System.err.println(logs2);
//    	RequestContext context = RequestContext.getCurrentInstance();  
        //update panel
    	
    	if(logs==null)
    	this.logs=logs2;
    	else
    	this.logs=logs+"\n"+logs2;
    	temp=logs;
    
    	return new ResponseEntity<String>(HttpStatus.OK);
    }
}	
