package com.setek.tr.centralwarehouse.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.services.TransferDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransferLazyModelForRobotFailed extends LazyDataModel<Transfer> {
    
    @Autowired
    private TransferDataService transferService;

//    public TransferLazyModelForRobotFailed(TransferDataService transferService) {
//        this.transferService = transferService;
//    }
    
    private Logger log = LoggerFactory.getLogger(TransferLazyModelForRobotFailed.class);

    @Override
    public List<Transfer> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        log.info("TransferLazyDataModel.load()");

        int totalDataSize = 0;

        log.info("TransferLazyDataModel.load() -> " + (filters != null && !filters.isEmpty()));

        if (filters != null && !filters.isEmpty()) {

            Map<String, Object> paramMaps = new LinkedHashMap<>();

            for (String key : filters.keySet()) {

                Object value = filters.get(key);

                log.info("load key : " + key + " value : " + value.toString());

                key = key.equals("artNo") ? "art_No" : key;
                key = key.equals("siparisAdet") ? "order_quantity" : key;
                key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
                key = key.equals("olusturmaTarihi") ? "create_date" : key;
                key = key.equals("sevkTarihi") ? "shipment_date" : key;
                key = key.equals("artDurum") ? "art_status" : key;

                if (key.equals("status")) {
                    value = Status.valueOf(value.toString()).getDescription();

                }

                paramMaps.put(key, value);

            }

            List<Transfer>  filteredList = transferService.retrieveStatus4ForFilter(paramMaps, first, pageSize, sortField, sortOrder);
//
            totalDataSize = transferService.retrieveTransferCountStatus4ForFilter(paramMaps).intValue();
            this.setRowCount(totalDataSize);

            return filteredList;

        }

        List<Transfer> transfers = transferService.retrieveTransfersStatus4(first, pageSize, sortField, sortOrder);

        totalDataSize = transferService.retrieveTransfersStatus4Count().intValue();

        this.setRowCount(totalDataSize);

        return transfers;

    }

}
