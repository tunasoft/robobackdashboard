package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.model.CentralWareHouseResponse;
import com.setek.tr.centralwarehouse.model.ServiceSyncInfo;
import com.setek.tr.centralwarehouse.persistance.ServiceSyncInfoRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

//@ViewScoped
@Controller
public class ServiceSyncController {

	@Autowired
	private ServiceSyncInfoRepo syncInfoRepo;

	private List<ServiceSyncInfo> syncInfos;

	private Logger log = LoggerFactory.getLogger(ServiceSyncController.class);

	@PostConstruct
	public void init() {
		syncInfos = syncInfoRepo.findAll();
	}


	public void syncStockService(ServiceSyncInfo info) {
		log.info("ServiceSyncController.syncStockService()");
		if (!info.isRunning()) {

			try {
                final String uri = "http://localhost:9091/syncService/syncStockService";
                RestTemplate restTemplate = new RestTemplate();
                CentralWareHouseResponse result = restTemplate.getForObject(uri, CentralWareHouseResponse.class);

                if (result.isSuccess()) {
                    FacesMessage message = new FacesMessage("Successful", "Stocks synchronized.");
                    FacesContext.getCurrentInstance().addMessage(null, message);

                } else {
                    FacesMessage message = new FacesMessage("UnSuccessful", "" + result.getMessage());
                    FacesContext.getCurrentInstance().addMessage(null, message);
				}
				syncInfos = syncInfoRepo.findAll();
			} catch (Exception e) {
				FacesMessage message = new FacesMessage("UnSuccessful", "" + e.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}

	}

	public List<ServiceSyncInfo> getSyncInfos() {
		return syncInfos;
	}

	public void setSyncInfos(List<ServiceSyncInfo> syncInfos) {
		this.syncInfos = syncInfos;
	}

}
