/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.constant.Conditions;
import com.setek.tr.centralwarehouse.constant.LastChanger;
import com.setek.tr.centralwarehouse.model.ResultOfRequestRobo;
import com.setek.tr.centralwarehouse.model.RobobackStatus;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.persistance.RobabackStatusRepo;
import com.setek.tr.centralwarehouse.services.ExcelService;
import com.setek.tr.centralwarehouse.services.TransferCancelOperationService;
import com.setek.tr.centralwarehouse.services.TransferDataService;
import com.setek.tr.centralwarehouse.services.TransferEditOperationService;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.PrimeFaces;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author ASUS
 */
@Getter
@Setter
@ViewScoped
@Controller
public class TransferController {

    private final Logger log = LoggerFactory.getLogger(TransferController.class);

    private boolean booleanVal;
    private Date startDate;
    private Date endDate;
    private List<Transfer> transfers;
    private List<String> orderNos;
    private List<Status> statusList;
    private TransferLazyDataModel transferLazyDataModel;
    private List<RobobackStatus> ekolStatusList;
    private List<String> ekolStatusNameList;
    private List<Conditions> conditionsList;
    private List<LastChanger> lastChangerList;
    private String reSendOrderNo;
    private boolean reSendRendered;
    private Map<String, String> filterProps;

    @Autowired
    private TransferDataService transferDataService;
    @Autowired
    private RobabackStatusRepo robabackStatusRepo;
    @Autowired
    private TransferCancelOperationService tcos;
    @Autowired
    private TransferEditOperationService teos;
    @Autowired
    private ExcelService excelService;


    @PostConstruct
    public void init() {

        log.info("TransferController.init()");

        initializeFilterProps();

        reSendRendered = false;

        statusList = Arrays.asList(Status.values());

        conditionsList = Arrays.asList(Conditions.values());

        lastChangerList = Arrays.asList(LastChanger.values());

        ekolStatusList = robabackStatusRepo.getEkolStatus();

        ekolStatusNameList = new ArrayList<>();

        ekolStatusList.forEach(status -> {
            ekolStatusNameList.add(status.getStatuName());
        });

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar beforeOneWeak = Calendar.getInstance();
        beforeOneWeak.add(Calendar.DAY_OF_MONTH, -2);
        try {
            beforeOneWeak.setTime(sdf.parse(sdf.format(beforeOneWeak.getTime())));
            startDate = beforeOneWeak.getTime();
            endDate = sdf.parse(sdf.format(new Date()));
        } catch (ParseException e) {
            log.error("TransferController.init() -> ", e);
        }

        log.info("TransferController.init()");
        transferLazyDataModel = new TransferLazyDataModel(transferDataService, startDate, endDate, ekolStatusList, filterProps);
        statusList = Arrays.asList(Status.values());
    }

    public void onChange(ValueChangeEvent ev) {

        SelectOneMenu select = (SelectOneMenu) ev.getSource();

        this.filterProps.replace(select.getId(), (String) ev.getNewValue());

    }

    private void initializeFilterProps() {

        String defaultValue = "=";

        filterProps = new HashMap<>();

        filterProps.put("siparisAdet", defaultValue);
        filterProps.put("siparisGirilen", defaultValue);
        filterProps.put("ekolGirilen", defaultValue);
        filterProps.put("siparisTransfer", defaultValue);

    }

    public List<SelectItem> getFilterOptions() {
        List<SelectItem> options = new ArrayList<SelectItem>();

        options.add(new SelectItem("=", "Equal", "Equal  '='"));
        options.add(new SelectItem(">", "Greater", "Greater  '>'"));
        options.add(new SelectItem("<", "Less", "Less  '<'"));
        options.add(new SelectItem(">=", "greater than", "Greater than '>='"));
        options.add(new SelectItem("<=", "Less Than", "Less than '<='"));
        options.add(new SelectItem("!=", "Not Equal", "Not Equal '!='"));
        options.add(new SelectItem("like", "Contains", "Contains 'like'"));

        return options;
    }

    public void setReSendRendered() {
        reSendRendered = !reSendRendered;
    }


    public void reSendOrderOperation() {
        RestTemplate restTemplate = new RestTemplate();

        String url = "http://localhost:9091/v1/ekol/operationdata/re-send";

        FacesContext context = FacesContext.getCurrentInstance();

        try {
            if (reSendOrderNo == null || reSendOrderNo.isEmpty()) {
                throw new IllegalArgumentException("Tekrar gönderim için şipariş numarası alanı dolu olmalıdır.");
            }

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("order-no", reSendOrderNo);

            HttpEntity<?> entity = new HttpEntity<>(headers);

            HttpEntity<ResultOfRequestRobo> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    entity,
                    ResultOfRequestRobo.class);

            ResultOfRequestRobo result = response.getBody();
            if (result != null && result.isRequestSuccess()) {
                context.addMessage("ReSend Order Operation", new FacesMessage(FacesMessage.SEVERITY_INFO, result.getRequestMessages() == null ? "" : result.getRequestMessages(), "Date - >" + result.getRequestDate() + "  TransActionCode -> " + result.getTransactionCode()));
                PrimeFaces.current().executeScript("PF('transferDataTable').filter()");

            } else {
                log.info("İletilemeyen sipariş -> {}", reSendOrderNo);
                context.addMessage("ReSend Order Operation", new FacesMessage(FacesMessage.SEVERITY_WARN, result.getRequestMessages() == null ? "" : result.getRequestMessages(), "Date - >" + result.getRequestDate() + "TransActionCode -> " + result.getTransactionCode()));
            }
        } catch (RestClientException e) {
            log.error("Could not connet to RobobackServer -> ", e);
            context.addMessage("ReSend Order Operation", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error ", "Sunucu ile bağlantı kurulamadı."));
        }
    }


    public void isOrderNoValid(FacesContext ctx, UIComponent component, String value) {
        if (value.length() < 9) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_INFO, "Validation Fail", "Sipariş numarası en az 9 karakter olmalı."));
        }

        if (!transferDataService.isExistTransferBySiparisNo(value)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Fail", "Sipariş numarasına ait kayıt bulunamadı."));
        }

        PrimeFaces.current().executeScript("document.getElementById('transfer:transferDataTable:orderNo:filter').setAttribute('value'," + value + ");");

        PrimeFaces.current().executeScript("PF('transferDataTable').filter()");

    }


    public void filterIt() throws ParseException {
        log.info("TransferController.filterIt() start");
        transferLazyDataModel = new TransferLazyDataModel(transferDataService, startDate, endDate, ekolStatusList, filterProps);
        log.info("TransferController.filterIt() end");
    }

    public List<Transfer> getTransfersbyId(String orderNo) {
        log.info("TransferController.getTransfersbyId() start order no : " + orderNo);
        List<Transfer> list = new ArrayList<Transfer>();

        for (Transfer transfer : transfers) {
            if (transfer.getSiparisNo().equals(orderNo)) {
                list.add(transfer);
            }
        }
        log.info("TransferController.getTransfersbyId() start end");
        return list;
    }

    public void exportTransfersToExcelinSomeRange(TransferLazyDataModel tld) {
        Workbook workbook;
        workbook = this.excelService.prepareExcelReportOfTransfersInSomeInterval(tld.getTrasnferDataForExcel());
        sendFileToBrowser(workbook);
    }

    public String sendFileToBrowser(Workbook workbook) {
        try {
            String filename = "transfers.xlsx";

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();

            externalContext.setResponseContentType(
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//			externalContext.setResponseContentType("application/vnd.ms-excel");

            // http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
            // attachment (pops up a "Save As" dialogue) or inline (let the web browser
            // handle the display itself)
            externalContext.setResponseHeader(
                    "Content-Disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(externalContext.getResponseOutputStream()); // get workbook
            facesContext.responseComplete(); // if I don't call responseComplete() => IllegalStateException

            return null; // remain on same page
        } catch (Exception e) {
            log.error("sendFileToBrowser -> " , e);
            return null; // remain on same page
        }
    }

    public String getStatuName(int statuCode) {

        for (RobobackStatus robobackStatus : ekolStatusList) {
            if (robobackStatus.getStatuCode() == statuCode) {
                return robobackStatus.getStatuName();
            }
        }
        return "";
    }

    public String getStatuColor(int statuCode) {

        for (RobobackStatus robobackStatus : ekolStatusList) {
            if (robobackStatus.getStatuCode() == statuCode) {
                return robobackStatus.getColorCode();
            }
        }
        return "";
    }

    public boolean isEditable(Transfer transfer) {

        if (transfer.getSiparisTipi().equals("ORDER")) {
            return (transfer.getStatus().equals(Status.SentToEkol));
        }

        return transfer.getStatus().equals(Status.SentToEkol) && transfer.getOrderTypeCode() == 10;

    }

}
