/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.controller;

import com.setek.tr.centralwarehouse.ServiceInfo;
import com.setek.tr.centralwarehouse.model.StockExcelReportDTO;
import com.setek.tr.centralwarehouse.model.StockRobo;
import com.setek.tr.centralwarehouse.persistance.StockRepository;
import com.setek.tr.centralwarehouse.services.ExcelService;
import com.setek.tr.centralwarehouse.services.StockService;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author ASUS
 */
@Getter
@Setter
@ViewScoped
@Controller
public class StockController implements Serializable {

    @Autowired
    StockService stockService;

    @Autowired
    private StockRepository sr;

    private List<StockRobo> stocks;

    private List<StockRobo> filteredStocks;

    private StockLazyDataModel stockLazyDataModel;

    private Date startDate;
    private Date endDate;

    @Autowired
    private ExcelService excelService;

    public StockController() {

    }

    @PostConstruct
    public void init() {

        SimpleDateFormat sdfToday = new SimpleDateFormat("yyyy-MM-dd");

        Calendar calendar = Calendar.getInstance();

        try {
            calendar.setTime(sdfToday.parse(sdfToday.format(new Date())));

            calendar.add(Calendar.DAY_OF_MONTH, -1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        startDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 2);
        endDate = calendar.getTime();

        startDate.setHours(0);
        startDate.setMinutes(0);
        endDate.setHours(23);
        endDate.setMinutes(59);


        stockLazyDataModel = new StockLazyDataModel(stockService, startDate, endDate);
    }

    public void export36StocksToExcel() {
        Workbook workbook;

        try {
            workbook = this.excelService.wareHouse36Excel(Arrays.asList(getDataFromServer()));

            sendFileToBrowser(workbook);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private StockExcelReportDTO[] getDataFromServer() {
        RestTemplate restTemplate = new RestTemplate();

        ServiceInfo serviceInfo = ServiceInfo.ROBOBACK_CLIENT;
        return restTemplate.getForObject("http://" + serviceInfo.getHostName() + ":" + serviceInfo.getPort() + "/syncService/getWarehouseExcelData", StockExcelReportDTO[].class);
    }

    public void exportStocksToExcel(StockLazyDataModel stockLazyDataModel) {
        Workbook workbook;

        workbook = this.excelService.prepareExcelReportOfTheStocks(stockLazyDataModel.load2(stockLazyDataModel.getFilters2()));

        sendFileToBrowser(workbook);
    }


    public Integer findStockDifference(Integer ekolStock, Integer dwhStock) {
        int difference = 0;

        if (ekolStock > dwhStock) {
            difference = ekolStock - dwhStock;
        } else {
            difference = dwhStock - ekolStock;
        }

        return difference;
    }

    public StockLazyDataModel getStockLazyDataModel() {
        return stockLazyDataModel;
    }

    public void setStockLazyDataModel(StockLazyDataModel stockLazyDataModel) {
        this.stockLazyDataModel = stockLazyDataModel;
    }

    public void filterIt() throws ParseException {
        stockLazyDataModel = new StockLazyDataModel(stockService, startDate, endDate);
    }

    public String sendFileToBrowser(Workbook workbook) {
        try {
            String filename = "stocks.xlsx";

            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();

            externalContext.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//			externalContext.setResponseContentType("application/vnd.ms-excel");

            // http://stackoverflow.com/questions/3592058/how-to-send-byte-as-pdf-to-browser-in-java-web-application
            // attachment (pops up a "Save As" dialogue) or inline (let the web browser
            // handle the display itself)
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

            workbook.write(externalContext.getResponseOutputStream()); // get workbook
            facesContext.responseComplete(); // if I don't call responseComplete() => IllegalStateException

            return null; // remain on same page
        } catch (Exception e) {
            // handle exception...
            return null; // remain on same page
        }
    }

}
