package com.setek.tr.centralwarehouse.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.enterprise.context.RequestScoped;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.stereotype.Controller;

@Controller
@RequestScoped
public class FileDownloadController {

	private StreamedContent file;

	public FileDownloadController() throws FileNotFoundException {

	}

	public StreamedContent getFile() {
		return file;
	}

	public StreamedContent saveFileAsPdf(String location) throws FileNotFoundException {
		InputStream stream = new FileInputStream(location);

		file = new DefaultStreamedContent(stream, "application/pdf", "pdfFile.pdf");
		return file;
	}

	public StreamedContent saveFileAsXls(String location) throws FileNotFoundException {
		InputStream stream = new FileInputStream(location);
		file = new DefaultStreamedContent(stream, "application/xls", "xlsFile.xls");
		return file;
	}

}
