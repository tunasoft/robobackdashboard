package com.setek.tr.centralwarehouse.controller;


import com.setek.tr.centralwarehouse.model.ArticleGroupName;
import com.setek.tr.centralwarehouse.services.ArticleGroupNameService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
@ViewScoped
@Controller
public class ArticleGroupNameController {


    @Autowired
    private ArticleGroupNameService articleGroupNameService;

    private List<ArticleGroupName> data = new ArrayList<>();

    private List<ArticleGroupName> ftd = new ArrayList<>();

    @PostConstruct
    public void init() {
        data = articleGroupNameService.fetchAllArticleGroupNames();
        ftd = data;
    }

    public boolean globalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.isEmpty()) {
            return true;
        }
        int filterInt = getInteger(filterText);

        ArticleGroupName art = (ArticleGroupName) value;
        return art.getGroup().toLowerCase().contains(filterText)
                || art.getGroupType().toLowerCase().contains(filterText)
                || art.getGroupCode() == filterInt;
    }

    private int getInteger(String filterText) {
        try {
            return Integer.parseInt(filterText);
        } catch (Exception e) {
            return 0;
        }
    }

    public void onRowEdit(RowEditEvent event) {
        articleGroupNameService.saveArticleGroup((ArticleGroupName) event.getObject());
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(event.getObject()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }


}
