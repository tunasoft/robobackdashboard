package com.setek.tr.centralwarehouse.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.setek.tr.centralwarehouse.model.ErrorLogs;
import com.setek.tr.centralwarehouse.services.ErrorLogsService;

public class LogsLazyModel extends LazyDataModel<ErrorLogs> {

    private static final long serialVersionUID = 1L;

    private ErrorLogsService errorLogsService;
    private Logger log = LoggerFactory.getLogger(LogsLazyModel.class);

    public LogsLazyModel(ErrorLogsService errorLogsService) {
        this.errorLogsService = errorLogsService;
    }

    @Override
    public List<ErrorLogs> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        int totalDataSize = 0;

        if (filters != null && !filters.isEmpty()) {

            Map<String, Object> paramMaps = new LinkedHashMap<>();

            for (String key : filters.keySet()) {

                Object value = filters.get(key);

                log.info("load ket : " + key + " value : " + value.toString());

                key = key.equals("serviceName") ? "service_name" : key;
                key = key.equals("errorDate") ? "error_date" : key;
                key = key.equals("summaryError") ? "summary_error" : key;
                key = key.equals("error") ? "error" : key;

                paramMaps.put(key, value);

            }

            List<ErrorLogs> filteredList = new ArrayList<ErrorLogs>();

            filteredList = errorLogsService.retrieveLogsByParam(paramMaps, first, pageSize, sortField, sortOrder);
//
            totalDataSize = errorLogsService.retrieveLogsCountByParam(paramMaps).intValue();
            this.setRowCount(totalDataSize);

            return filteredList;

        }

        List<ErrorLogs> logs = errorLogsService.retrieveLogs(first, pageSize, sortField, sortOrder);

        totalDataSize = errorLogsService.retrieveLogsCount().intValue();
        this.setRowCount(totalDataSize);

        return logs;

    }

}
