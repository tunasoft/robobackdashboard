package com.setek.tr.centralwarehouse.controller;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.setek.tr.centralwarehouse.model.FilePojo2;


@Controller
@RequestScoped
public class FileController {
	Logger logger = LoggerFactory.getLogger(FileController.class);
	
	public FileController() {	text = "";
	filePojos = new ArrayList<>();
	xlsFiles = new ArrayList<>();
	pdfFiles = new ArrayList<>();
	list = new ArrayList<>();
	list2 = new ArrayList<>();
	list3 = new ArrayList<>();
	findTxt();
	findPdf();
	findXls();
	try {
		readTxt();
		readPdf();
		readXls();
	} catch (IOException e) {
		logger.error("File Controller Exception :" , e);
	}
}
	private List<File> list;
	private List<File> list2;
	private List<File> list3;
	public List<File> getList2() {
		return list2;
	}
	public void setList2(List<File> list2) {
		this.list2 = list2;
	}
	public List<File> getList3() {
		return list3;
	}
	public void setList3(List<File> list3) {
		this.list3 = list3;
	}
	private String text;
	private String textTitle;

	public String getText() {
		return text;
	}
	@PostConstruct
	public void init() {
		
	text = "";
	filePojos = new ArrayList<>();
	xlsFiles = new ArrayList<>();
	pdfFiles = new ArrayList<>();
	list = new ArrayList<>();
	list2 = new ArrayList<>();
	list3 = new ArrayList<>();
	findTxt();
	findPdf();
	findXls();
	try {
		readTxt();
		readPdf();
		readXls();
	} catch (IOException e) {
		logger.error("File Controller init Exception :" , e);
	}

	}
	
	
	
	public void setText(String text) {
		this.text = text;
	}
	public String getTextTitle() {
		return textTitle;
	}
	public void setTextTitle(String textTitle) {
		this.textTitle = textTitle;
	}
	public List<FilePojo2> getFilePojos() {
		return filePojos;
	}
	public void setFilePojos(List<FilePojo2> filePojos) {
		this.filePojos = filePojos;
	}
	private List<FilePojo2> filePojos;
	private List<FilePojo2> xlsFiles;
	private List<FilePojo2> pdfFiles;
	public List<FilePojo2> getXlsFiles() {
		return xlsFiles;
	}
	public void setXlsFiles(List<FilePojo2> xlsFiles) {
		this.xlsFiles = xlsFiles;
	}
	public List<FilePojo2> getPdfFiles() {
		return pdfFiles;
	}
	public void setPdfFiles(List<FilePojo2> pdfFiles) {
		this.pdfFiles = pdfFiles;
	}
	public void findTxt() {

		File directoryPath = new File("C:\\mrRobot\\");
		File[] files = directoryPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".txt");
			}
		});

		for (int i = files.length - 1; i >= 0; i--) {
			list.add(files[i]);
		}	

	}
	public void findPdf() {

		File directoryPath = new File("C:\\mrRobot\\");
		File[] files = directoryPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".pdf");
			}
		});

		for (int i = files.length - 1; i >= 0; i--) {
			list2.add(files[i]);
		}	

	}
	public void findXls() {

		File directoryPath = new File("C:\\mrRobot\\");
		File[] files = directoryPath.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xls");
			}
		});

		for (int i = files.length - 1; i >= 0; i--) {
			list3.add(files[i]);
		}	

	}
	public List<File> getList() {
		return list;
	}
	public void setList(List<File> list) {
		this.list = list;
	}
	
	
	public void readPdf() throws IOException {
		pdfFiles = new ArrayList<FilePojo2>();
		for (int index = 0; index < list2.size(); index++) {
			String x=list2.get(index).toString();
			x =x.substring(11,x.length());
			pdfFiles.add(new FilePojo2(x, null,list2.get(index).toString()));
		}
		}
	
	public void readXls() throws IOException {
		xlsFiles = new ArrayList<FilePojo2>();
		for (int index = 0; index < list3.size(); index++) {
			String x=list3.get(index).toString();
			x =x.substring(11,x.length());
			xlsFiles.add(new FilePojo2(x, null,list3.get(index).toString()));
		}
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void readTxt() throws IOException {
		filePojos = new ArrayList<FilePojo2>();
		for (int index = 0; index < list.size(); index++) {

			setTextTitle(list.get(index).toString());
			File file = new File(textTitle);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			text = "";
			String line = bufferedReader.readLine();
			while (line != null) {

				text += line + "\n";
				line = bufferedReader.readLine();

			}

			filePojos.add(new FilePojo2(list.get(index).toString(), text,null));
             bufferedReader.close();
		}
	}
}
