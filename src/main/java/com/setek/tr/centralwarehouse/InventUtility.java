package com.setek.tr.centralwarehouse;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InventUtility {

    public static List<String> invalidLines(List<String> rows) {
        List<String> lInvalidLines = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

        int i = 2;
        for (String line : rows) {

            String[] cells = line.split("\\$");

            String toOutletId;
            String goodsSenderOutletId;
            String goodsReceiverOutletId;
            String deliveryDate;
            String bookingDate;
            String productId;
            String quantity;

            toOutletId = cells[0];
            goodsSenderOutletId = cells[0];
            goodsReceiverOutletId = cells[2];
            deliveryDate = sdf.format(new Date());
            bookingDate = deliveryDate;
            productId = cells[7];
            quantity = cells[8];


            if (toOutletId.equals("0") || goodsSenderOutletId.equals("0")
                    || goodsReceiverOutletId.equals("0") || productId.equals("0") || quantity.equals("0")
                    || deliveryDate.equals("0") || bookingDate.equals("0")) {

                lInvalidLines.add("File line number : " + i + " -> " + line.replace("$", " | "));
                i++;
                continue;
            }


            try {
                sdf.parse(bookingDate);
                sdf.parse(deliveryDate);
            } catch (Exception e) {
                lInvalidLines.add("File line number : " + i + " -> " + line.replace("$", " | "));
                i++;
                continue;
            }
            i++;
        }

        return lInvalidLines;
    }


    public static boolean checkFileIsExistAndCreate(String filePath) {

        boolean result = false;

        if (filePath == null) {
            return result;
        }

        result = new File(filePath).exists();

        if (!result) {
            result = new File(filePath).mkdir();
        }

        return result;
    }

}
