/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.RestClients;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class RobobackResponse {

    private boolean success;

    private String messages;

    private Map<String, Object> responseData;

    public RobobackResponse(boolean success, String messages) {
        this.success = success;
        this.messages = messages;
        this.responseData = new HashMap<>();
    }

    public RobobackResponse() {
        this.responseData = new HashMap<>();
        this.messages = "";
        this.success = false;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Map<String, Object> getResponseData() {
        return responseData;
    }

    public void setResponseData(Map<String, Object> responseData) {
        this.responseData = responseData;
    }
}
