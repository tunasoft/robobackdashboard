package com.setek.tr.centralwarehouse.RestClients;

import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MrRobotClient {

	private Logger log = LoggerFactory.getLogger(MrRobotClient.class);

	
	public boolean sendCancelDataToRobot(LinkedHashMap<String, List<String>>  cancelMapForRobot) {

		final String url = "http://localhost:8083/mrrobotclientservice/mrRobotCentralWareHouseSetCancelList";

		RestTemplate restTemplate = new RestTemplate();
		Boolean result = restTemplate.postForObject(url, cancelMapForRobot, Boolean.class);
		if (result) {
			log.info("cancel data send to robot");
		}else {
			log.info("cancel data does not send to robot");
			
		}
		
		
		return false;
		
	}
	
}
