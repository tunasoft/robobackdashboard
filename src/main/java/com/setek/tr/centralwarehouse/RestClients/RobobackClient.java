package com.setek.tr.centralwarehouse.RestClients;

import com.setek.tr.centralwarehouse.model.Transfer;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RobobackClient {

    private final Logger log = LoggerFactory.getLogger(RobobackClient.class);

    public boolean updateTransfersByEkol() {

        final String url = "http://localhost:9091/v1/ekol/operationdata/updateTransfersByEkol";

        RestTemplate restTemplate = new RestTemplate();

        Boolean result = restTemplate.getForObject(url, Boolean.class);

        return result;
    }


    public RobobackResponse sendCancelRequestForAllOrder(String orderNo) {

        log.info("com.setek.tr.CentrolWareHouse.RestClients.RobobackClient.sendCancelRequestForAllOrder() start");
        RobobackResponse result = new RobobackResponse();
        try {
            String url = "http://localhost:9091/v1/ekol/operationdata/sendCancelRequest/";

            RestTemplate restTemplate = new RestTemplate();

            Map<String, Object> data = new HashMap<>();
            data.put("orderNo", orderNo);

            result = restTemplate.postForObject(url, data, RobobackResponse.class);

            log.info("TransferController.startCancelProcess() result -> {}", result);
        } catch (RestClientException e) {
            result = new RobobackResponse();
            result.setSuccess(false);
            result.setMessages(String.format("sendCancelRequestForAllOrder() ->  %s", ExceptionUtils.getFullStackTrace(e)));
            log.error(e.getMessage(), ExceptionUtils.getFullStackTrace(e));
        }
        log.info("com.setek.tr.CentrolWareHouse.RestClients.RobobackClient.sendCancelRequestForAllOrder() end");

        return result;
    }


    public RobobackResponse sendCancelRequestForLines(String orderNo, List<String> articleList) {

        log.info("RobobackClient.sendCancelRequestForAllOrder start");

        RobobackResponse result = new RobobackResponse();
        try {
            final String url = "http://localhost:9091/v1/ekol/operationdata/sendGoodsInCancel/";

            RestTemplate restTemplate = new RestTemplate();

            Map<String, Object> data = new HashMap<>();
            data.put("orderNo", orderNo);
            data.put("cancelLineList", articleList);

            result = restTemplate.postForObject(url, data, RobobackResponse.class);
            log.info("RobobackClient.sendCancelRequestForAllOrder  result -> {}", result);
        } catch (RestClientException e) {
            result = new RobobackResponse();
            result.setSuccess(false);
            result.setMessages(String.format("sendCancelRequestForLines() -> %s", ExceptionUtils.getFullStackTrace(e)));
            log.error(e.getMessage(), ExceptionUtils.getFullStackTrace(e));
        }

        log.info("RobobackClient.sendCancelRequestForAllOrder end");

        return result;
    }


    public Boolean isReturnDataServiceWorking() {
        System.out.println("com.setek.tr.CentrolWareHouse.RestClients.RobobackClient.isReturnDataServiceWorking()");
        final String url = "http://localhost:9091/v1/ekol/operationdata/isReturnDataServiceWorking";

        RestTemplate restTemplate = new RestTemplate();

        RobobackResponse result = restTemplate.getForObject(url, RobobackResponse.class);
        if (result != null) {
            return result.isSuccess();
        }

        return false;

    }

    public RobobackResponse getCurrentStatusOfTransferFromEkol(Transfer transfer) {

        final String url = "http://localhost:9091/v1/ekol/operationdata/currentStatusOfTransferFromEkol/";

        RestTemplate restTemplate = new RestTemplate();

        Map<String, String> uriVariables = new HashMap<>();

        uriVariables.put("transferNo", transfer.getSiparisNo());
        uriVariables.put("orderType", transfer.getSiparisTipi());

        RobobackResponse robobackResponse = restTemplate.postForObject(url, uriVariables, RobobackResponse.class);
        //       test sample data
        return robobackResponse;
    }


    public RobobackResponse sendCancelProcessForGoodsIn(String orderNo) {
        RobobackResponse result = new RobobackResponse();
        try {
            final String url = "http://localhost:9091/v1/ekol/operationdata/sendGoodsInCancel/";

            RestTemplate restTemplate = new RestTemplate();

            Map<String, String> uriVariables = new HashMap<>();

            uriVariables.put("asnNr", orderNo);

            result = restTemplate.postForObject(url, uriVariables, RobobackResponse.class);

            return result;
        } catch (RestClientException e) {
            result.setSuccess(false);
            result.setMessages("Fail ->  " + ExceptionUtils.getFullStackTrace(e));
            log.error(e.getMessage(), ExceptionUtils.getFullStackTrace(e));
        }
        return result;
    }

    public RobobackResponse sendCancelProcessForGoodsOut(String orderNo) {

        log.info("com.setek.tr.CentrolWareHouse.RestClients.RobobackClient.sendCancelProcessForGoodsOut() start");
        RobobackResponse result = new RobobackResponse();
        try {
            final String url = "http://localhost:9091/v1/ekol/operationdata/sendGoodsOutCancel/";

            RestTemplate restTemplate = new RestTemplate();

            result = restTemplate.postForObject(url, orderNo, RobobackResponse.class);

        } catch (RestClientException e) {
            result.setSuccess(false);
            result.setMessages("Fail ->  " + ExceptionUtils.getFullStackTrace(e));
            log.error(e.getMessage(), ExceptionUtils.getFullStackTrace(e));
        }
        log.info("com.setek.tr.CentrolWareHouse.RestClients.RobobackClient.sendCancelProcessForGoodsOut() end");

        return result;
    }

}
