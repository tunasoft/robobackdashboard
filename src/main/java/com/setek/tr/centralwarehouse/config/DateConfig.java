package com.setek.tr.centralwarehouse.config;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;
import java.util.Date;

@Configuration
public class DateConfig {


    //If this was passed with MONTH, it would return 1 Mar 2002 0:00:00.000.
    @Bean
    public Date dateConf(){
        return DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
    }
}
