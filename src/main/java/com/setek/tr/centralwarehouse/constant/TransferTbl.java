package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class TransferTbl {

    public static final String ID = "id";

    public static final String ORDER_TYPE = "order_type";

    public static final String ORDER_NO = "order_no";

    public static final String RETRIEVE_TRANSFER_NO = "retrieve_transfer_no";

    public static final String ORDER_STATUS = "order_status";

    public static final String LAST_CHANGER = "last_changer";

    public static final String CREATE_DATE = "create_date";

    public static final String SUPPLIER_NO = "supplier_no";

    public static final String SUPPLIER_NAME = "supplier_name";

    public static final String SHIPMENT_DATE = "shipment_date";

    public static final String STATUS = "status";

    public static final String ART_APP_STATUS = "art_app_status";

    public static final String EKOL_STATUS = "ekol_status";

    public static final String DELIVERY_NO = "delivery_no";

    public static final String ART_NO = "art_no";

    public static final String ART_STATUS = "art_status";

    public static final String ORDER_QUANTITY = "order_quantity";

    public static final String ENTERED_QUANTITY_BY_EKOL = "entered_quantity_by_ekol";

    public static final String ENTERED_QUANTITY_BY_STORE = "entered_quantity_by_store";

    public static final String QUANTITY_TRANSFER = "quantity_transfer";

    public static final String EXTERNAL_ORDER_NO = "external_order_no";

    public static final String EKOL_SHIPMENT_DATE = "ekol_shipment_date";

    public static final String PROCESS_DATE = "process_date";

    public static final String PARTIALLY_PROCESSED = "partially_processed";

    public static final String ORDER_TYPE_CODE = "order_type_code";

    public static final String ROBOTIC_TRY_TIME = "robotic_try_time";

    public static final String EDITED_QUANTITY = "edited_quantity";
}
