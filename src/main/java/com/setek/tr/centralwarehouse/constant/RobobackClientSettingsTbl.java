package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class RobobackClientSettingsTbl {

    public static final String ID = "id";

    public static final String ACTIVE = "active";

    public static final String WORK_DAILY ="work_daily";

    public static final String WORKING_HOUR = "working_hour";

    public static final String WORK_HOURLY = "work_hourly";

    public static final String WORKING_MINUTE = "working_minute";

    public static final String WORK_PERIODICALLY = "work_periodically";

    public static final String SERVICE_NAME = "service_name";

    public static final String WORK_BY_SPECIFIED_TIME = "work_by_specified_time";

    public static final String VERSION ="version";

    public static final String WORKING_HOURS = "working_hours";

    public static final String RUNNING ="running";
}
