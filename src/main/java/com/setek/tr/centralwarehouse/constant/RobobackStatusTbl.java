package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class RobobackStatusTbl {

    public static final String ID = "id";

    public static final String COLOR_CODE = "color_code";

    public static final String STATUS_CODE = "status_code";

    public static final String STATUS_NAME = "status_name";

    public static final String STATUS_TYPE = "status_type";
}
