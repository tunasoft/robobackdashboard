package com.setek.tr.centralwarehouse.constant;


import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract  class TransferOperationLogsTbl {

    public static final String ID = "id";

    public static final String ART_NO = "art_no";

    public static final String EDITED_QUANTITY = "edited_quantity";

    public static final String PROCESS_TYPE = "process_type";

    public static final String QUANTITY = "quantity";

    public static final String REQUEST_DATE = "request_date";

    public static final String RESPONSE = "response";

    public static final String RESPONSE_DATE ="response_date";

    public static final String RP_TRANSACTION_CODE ="rp_transaction_code";

    public static final String RP_MESSAGE = "rp_message";

    public static final String RQ_MESSAGE = "rq_message";

    public static final String RQ_TRANSACTION_CODE = "RQ_TRANSACTION_CODE";

    public static final String TRANSFER_NO =  "transfer_no";

    public static final String TRANSFER_TYPE = "transfer_type";

    public static final String USER_NAME = "user_name";



}
