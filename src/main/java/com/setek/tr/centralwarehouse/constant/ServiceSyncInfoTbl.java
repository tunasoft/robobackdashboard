package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class ServiceSyncInfoTbl {

    public static final String ID = "id";

    public static final String END_DATE = "end_date";

    public static final String RUNNING = "running";

    public static final String SERVICE_NAME = "service_name";

    public static final String START_DATE = "start_date";
}
