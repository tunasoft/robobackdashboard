package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class UpsertTransferOrderTbl {

    public static final String ID = "id";

    public static final String ART_STOCK = "art_stock";

    public static final String BOOKING_DATE = "booking_date";

    public static final String CONTROL_COUNT = "control_count";

    public static final String DELIVERY_DATE = "delivery_date";

    public static final String DELIVERY_SIGN = "delivery_sign";

    public static final String EXTERNAL_ORDER_ID = "external_order_id";

    public static final String FROM_STOCK_AREA_NAME_ID = "from_stock_area_name_id";

    public static final String GOODS_RECEIVER_OUTLET_ID = "goods_receiver_outlet_id";

    public static final String GOODS_SENDER_OUTLET_ID = "goods_sender_outlet_id";

    public static final String INSERTED_DATE = "inserted_date";

    public static final String IS_SUCCESS = "is_success";

    public static final String IS_VALID = "is_valid";

    public static final String POSITION_ID = "position_id";

    public static final String PROCESS_DATE = "process_date";

    public static final String PRODUCT_ABT_NO = "product_abt_no";

    public static final String PRODUCT_GROUP_NAME = "product_group_name";

    public static final String PRODUCT_ID = "product_id";

    public static final String QUANTITY = "quantity";

    public static final String SENDER_MESSAGE_ID = "sender_message_id";

    public static final String SOURCE_SYSTEM = "source_system";

    public static final String SOURCE_SYSTEM_ORDER_ID = "source_system_order_id";

    public static final String STATUS = "status";

    public static final String SUPPLIER_NAME = "supplier_name";

    public static final String SUPPLIER_NO = "supplier_No";

    public static final String TO_OUTLET_ID = "to_outlet_id";

    public static final String TRANSFER_ACTION = "transfer_action";

    public static final String TRANSFER_REQUEST = "transfer_request";

    public static final String TRANSFER_SUB_TYPE = "transfer_sub_type";

}
