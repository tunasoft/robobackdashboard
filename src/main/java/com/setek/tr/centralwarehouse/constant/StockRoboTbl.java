package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract  class StockRoboTbl {

    public static final String ID = "id";

    public static final String ARTICLE_CODE = "article_code";

    public static final String EKOL_STOCK = "ekol_stock";

    public static final String DWH_STOCK = "dwh_stock";

    public static final String CONTROL_DATE = "control_date";

    public static final String STATUS = "status";

    public static final String EKOL_STOCK_IN_PLANNED = "ekol_stock_in_planned";

    public static final String EKOL_STOCK_OUT_PLANNED = "ekol_stock_out_planned";

    public static final String EKOL_EXPECTED_STOCK = "ekol_expected_stock";

    public static final String MATCHED = "matched";

    public static final String STOCK_DIFFERENCE = "StockDifference";

}
