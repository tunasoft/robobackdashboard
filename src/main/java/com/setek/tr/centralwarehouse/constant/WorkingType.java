package com.setek.tr.centralwarehouse.constant;

public enum WorkingType {

    HOURLY("Runs one specified minute per hour", "Hourly"),
    PERIODICALLY("Runs every specified minute", "Periodically"),
    SPECIFIED_TIMES("Runs on selected hours", "Specified times");

    private final String description;

    private final String name;

    WorkingType(String description, String name) {
        this.description = description;
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

}
