package com.setek.tr.centralwarehouse.constant;

public enum LastChanger {

        TR_COBANT("TR-COBANT"),
        TR_ERTURKG("TR-ERTURKG"),
        DOGANOK("DOGANOK"),
        TR_UCUNCU("TR-UCUNCU"),
        TR_YENICE("TR-YENICE"),
        TR_HORASANG("TR-HORASANG"),
        TR_CAM("TR-CAM"),
        TR_PUNCHER("TR-PUNCHER"),
        TR_ALTINTAS("TR-ALTINTAS"),
        TR_YILMAZMET("TR-YILMAZMET"),
        TR_OZGENC("TR-OZGENC"),
        TRANSFER_MODULE("TRANSFER-MODULE"),
        SALDO_WB_VERTEIL("SALDO-WB-VERTEIL"),
        TR_GUNELC("TR-GUNELC"),
        TR_KOSEIL("TR-KOSEIL"),
        TR_KARAKOCF("TR-KARAKOCF"),
        TR_SAYGILIG("TR-SAYGILIG"),
        TR_ORKMEZ("TR-ORKMEZ"),
        TR_GURSOY("TR-GURSOY"),
        TR_DEMIRCISA("TR-DEMIRCISA"),
        TR_BASTUGH("TR-BASTUGH"),
        RELEX_REPLENISHMENT("RELEX-REPLENISHMENT");

        private final String description;

        LastChanger(String description){
            this.description = description;
        }


}
