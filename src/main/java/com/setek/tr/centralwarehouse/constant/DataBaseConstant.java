package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DataBaseConstant {

    @NoArgsConstructor
    public static class Schemas {
        public static final String DBO = "dbo";
    }

    public class ACTION {

        public static final String ID = "id";
    }


    @NoArgsConstructor
    public class Tables {


        public static final String TRANSFER = "transfer";

        public static final String ARTICLE_GROUP = "article_group";

        public static final String ARTICLE_ROBO = "article_robo";

        public static final String ARTICLE_SERIAL_INFO = "article_serial_info";

        public static final String ARTICLE_MEDIA_SATURN_ROBO = "article_media_saturn_robo";

        public static final String CANCEL_OPERATION_LOG = "cancel_operation_log";

        public static final String COMPANY_ROBO = "company_robo";

        public static final String ERROR_LOGS = "error_logs";

        public static final String GOODS_IN_CONFIRMATION_ROBO = "goods_in_confirmation_robo";

        public static final String GOODS_IN_RETURN_LINE_ROBO = "goods_in_return_line_robo";

        public static final String GOODS_IN_RETURN_ROBO = "goods_in_return_robo";

        public static final String GOODS_IN_LINE_ROBO = "goods_in_line_robo";

        public static final String GOODS_IN_ROBO = "goods_in_robo";

        public static final String GOODS_OUT_CANCEL_ROBO = "goods_out_cancel_robo";

        public static final String GOODS_OUT_LINE_ROBO = "goods_out_line_robo";

        public static final String GOODS_OUT_RETURN_LINE_ROBO = "goods_out_return_line_robo";

        public static final String GOODS_OUT_RETURN_ROBO = "goods_out_return_line_robo";

        public static final String GOODS_OUT_ROBO = "goods_out_robo";

        public static final String LIEF = "lief";

        public static final String ROBOBACK_CLIENT_SETTINGS = "roboback_client_settings";

        public static final String ROBOBACK_STATUS = "roboback_status";

        public static final String SERVICE_SYNC_INFO = "service_sync_info";

        public static final String STOCK_ROBO = "stock_robo";

        public static final String TEST_TABLE = "test_table";

        public static final String TRANSFER_OPERATION_LOG = "transfer_operation_log";

        public static final String UPSERT_TRANSFER_ORDER = "upsert_transfer_order";

        public static final String USERS = "users";

        public static final String DYNAMIC_ARTICLE_GROUP = "dynamic_article_group";
    }


}
