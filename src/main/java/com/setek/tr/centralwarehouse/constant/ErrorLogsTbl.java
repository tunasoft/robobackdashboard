package com.setek.tr.centralwarehouse.constant;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class ErrorLogsTbl {

    public static final String ID = "id";

    public static final String ERROR = "error";

    public static final String ERROR_DATE = "error_date";

    public static final String METHOD_NAME = "method_name";

    public static final String SERVICE_NAME = "service_name";

    public static final String SUMMARY_ERROR = "summary_error";

    public static final String ERROR_DEFINITION = "error_definition";

    public static final String ERROR_LEVEL = "error_level";

    public static final String MAIL_SENDED_TRY_COUNT = "mail_sended_try_count";

    public static final String MAIL_SENDED = "mail_sended";

}
