package com.setek.tr.centralwarehouse.constant;

public enum ServiceName {
    COMPANY_SERVICE("Company Service", "Sends company info to ekol"), GOODS_OUT_SERVICE("Goods Out Service", "This service handles goods out (shipping, return, update)"), GOODS_IN_SERVICE("Goods In Service", "This service handles goods in (shipping, return, update)"), ARTICLE_SERVICE("Article Service", "Sends article information to Ekol"), STOCK_SERVICE("Stock Service", "This service sync stock information between Ekol and MediaMarkt"), RETURN_DATA_SERVICE("Return Data Service", "This service processes return data from the ekol."), PUSH_TRANSFER_SERVICE("Push Transfer Service", "This service creates the orders specified in the replenishment at the relevant store."), ERROR_REPORT_SERVICE("Error Report Service", "This service notifies errors in the system via e-mail."), INVENT_STOCK_SERVICE("Invent Stock Service", ""), GOODS_OUT_DAILY_REPORT_SERVICE("Goods Out Daily Report", "Sends information about goods out by e-mail"), UNPAIRED_STOCKS_SERVICE("Unpaired Stocks Service", "Sends information about unpaired stocks by e-mail"), ROBOTIC_START_SERVICE("Robotic Start Service", "Robot starter "), DAILY_MIN_STOCK_REPORT("Daily Min Stock Report", "Sends a daily minimum stock report by e-mail"), GOODS_IN_DAILY_REPORT_SERVICE("Goods In Daily Report Service", "Sends information about goods in orders by e-mail"), REPLENISHMENT_ORDERS_REPORT_SERVICE("Replenishment Orders Report Service", "send replenishment excel report mail."),
    DAILY_BROKEN_STOCK_REPORT("Daily report for broken stock", "Send Excel for daily broken stock"), ReplenishmentOrdersByStoresandDepartmentsReport("ReplenishmentReport", "Replenishment Report");


    private final String displayName;
    private final String description;

    ServiceName(String displayName, String description) {
        this.description = description;
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getDescription() {
        return this.description;
    }

}
