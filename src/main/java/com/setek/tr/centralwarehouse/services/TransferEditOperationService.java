/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.RestClients.RobobackResponse;
import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.model.TransferOperationLog;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ASUS
 */
@Setter
@Getter
@Service
public class TransferEditOperationService extends TransferOptionsService {

    private final Logger log = LoggerFactory.getLogger(TransferEditOperationService.class);

    private List<Transfer> transferList;
    private List<Transfer> selectedTransferList;

    private Transfer selectedTransfer;
    private boolean isRowEdit;

    @Override
    public void openDialog(Transfer transfer) {
        selectedTransfer = transfer;
        transferList = getTransferDataService().findTransfersBySiparisNo(transfer.getSiparisNo());
        transferList = transferList.stream().filter(t -> !t.getStatus().equals(Status.Cancel)).collect(Collectors.toList());
    }

    /**
     *
     */
    @Override
    public void startProcess() {
        log.info("TransferEditOperationService.startProcess");
        if (!selectedTransferList.isEmpty()) {
            List<String> articleList = selectedTransferList.stream().map(t -> Integer.toString(t.getArtNo())).collect(Collectors.toList());

            RobobackResponse response = getRobobackClient().sendCancelRequestForLines(selectedTransferList.get(0).getSiparisNo(), articleList);

            if (response.isSuccess()) {
                selectedTransferList.forEach(t -> t.setStatus(Status.Cancel));
                getTransferDataService().saveTransfers(selectedTransferList);
                FacesMessage message = new FacesMessage("Successful", selectedTransfer.getSiparisNo() + " -> " + " Selected articles cancelled.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } else {
                FacesMessage message = new FacesMessage("Failed", selectedTransfer.getSiparisNo() + "Selected articles couldn't cancel." + response.getMessages());
                FacesContext.getCurrentInstance().addMessage(null, message);
            }

            TransferOperationLog opLog = createOperationLog(response, selectedTransfer.getSiparisNo(), selectedTransfer.getSiparisTipi());
            saveCancelOperationLog(opLog);

        } else {
            FacesMessage message = new FacesMessage("UnSuccessful", selectedTransfer.getSiparisNo() + " -> " + "You must select any line.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }


        closeDialog();
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('orderEditDialog').hide();");

    }

    @Override
    protected boolean isAvailableForProcess(Integer status) {
        if (status == null) {
            return false;
        }
        return ((selectedTransfer.getSiparisTipi().equals("ORDER") && status != 1));
    }

    public void onRowEdit(Transfer transfer) {
        log.info("TransferOptionsService.onRowEdit()");
        isRowEdit = true;
//        editedTransferList.add(transfer);
    }

    public void removeFromEditList() {
        log.info("TransferOptionsService.removeFromEditList()");
        selectedTransferList.forEach((transfer) -> {
            System.out.println(transfer.getArtNo());
            transfer.setStatus(Status.CancelRequest);
        });

        if (transferList.isEmpty()) {
            FacesMessage mess = new FacesMessage("UnSaved", "Can not be delete all line in edit mode. If you want to cancel ORDER please click 'Order Cancel' button.");
            FacesContext.getCurrentInstance().addMessage(null, mess);
            transferList.addAll(selectedTransferList);
        }
        selectedTransferList = new ArrayList<>();

        log.info("TransferOptionsService.removeFromEditList() edit transfer list size " + selectedTransferList.size());

    }


    @Override
    public void closeDialog() {
        transferList = null;
        selectedTransfer = null;
    }
}
