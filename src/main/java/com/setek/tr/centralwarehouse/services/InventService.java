package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.InventUtility;
import com.setek.tr.centralwarehouse.commonTools.ExcelReaderService;
import com.setek.tr.centralwarehouse.model.PushTransferDTO;
import com.setek.tr.centralwarehouse.model.UpsertTransferOrder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Getter
public class InventService {

    private final Logger logger = LoggerFactory.getLogger(InventService.class);


    private File uploadedFile;
    private int totalArticleOrderQuantity;
    private int storeCount;
    private int articleCount;
    private int totalLine;
    private String informationDialogMessage;
    private String transferAction = "CREATE_AND_ASSIGN";
    private List<UpsertTransferOrder> list;

    private String resultUploadedUpsertTransferOrderMessage;

    private List<UpsertTransferOrder> resultUploadedUpsertTransferOrders;

    private final Environment env;

    private List<String> invalidLines;
    private List<HashMap<String, Integer>> insufficientStock;
    private List<LinkedHashMap<String, String>> notComposedLines;

    private static final String REPLENISHMENT_EXCEL_XLSX = "Replenishment_excel.xlsx";
    private static final String REPLENISHMENT_STORE_EXCEL_XLSX = "Replenishment_store_excel.xlsx";


    public void setTransferAction(String transferAction) {
        this.transferAction = transferAction;
    }

    public void resetValues() {
        articleCount = 0;
        totalLine = 0;
        storeCount = 0;
        totalArticleOrderQuantity = 0;
        uploadedFile = null;
        transferAction = "CREATE_AND_ASSIGN";
    }


    public void handleFileUpload(FileUploadEvent event) {

        logger.info("File upload starting for -> {}", event.getFile().getFileName());
        UploadedFile file = event.getFile();

        FacesContext facesContext = FacesContext.getCurrentInstance();

        try {
            checkFileName(file.getFileName());

            writeFile(file);

            ExcelReaderService excelReaderService = new ExcelReaderService(uploadedFile);

            List<String> lineList = excelReaderService.getRows();

            invalidLines = getInvalidLine(lineList);
            boolean valid = invalidLines.isEmpty();
            if (!valid) {
                cancelInvent();
                PrimeFaces current = PrimeFaces.current();
                current.executeScript("PF('errorLineDialog').show();");
                return;
            }
            list = getUpsertTransfersFromFile(uploadedFile.getPath());
            createReportData(list);

            PrimeFaces.current().executeScript("PF('resultDialog').show();");


        } catch (Exception e) {
            logger.error("Error in HandleFileUpload -> ", e);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, e.getClass().getName(), e.getMessage()));
        }

    }


    private void writeFile(UploadedFile uploadedFile) throws Exception {

        String repUploadFilePath = env.getProperty("replenishmentUploadFolderPath");

        if (!InventUtility.checkFileIsExistAndCreate(repUploadFilePath)) {
            throw new FileNotFoundException("Folder cant create -> " + repUploadFilePath);
        }

        uploadedFile.write(repUploadFilePath + uploadedFile.getFileName());

        this.uploadedFile = new File(repUploadFilePath + uploadedFile.getFileName());

        if (!this.uploadedFile.exists()) {
            throw new FileNotFoundException("Error on file reading please check Shared folder");
        }

    }

    private void checkFileName(String fileName) throws NamingException {
        if (!(REPLENISHMENT_EXCEL_XLSX.equals(fileName) || REPLENISHMENT_STORE_EXCEL_XLSX.equals(fileName)))
            throw new NamingException("File name not supported please check");
    }


    public void openResultDialog() {
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('resultDialog').show();");
    }


    private List<String> getInvalidLine(List<String> lines) {
        return InventUtility.invalidLines(lines);
    }

    public List<UpsertTransferOrder> getUpsertTransfersFromFile(String filePath) throws UnsupportedEncodingException {

        final String url = "http://localhost:9091/mediaSaturnController/getUpsertTransfersFromInvent?filePath="
                + filePath;

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<UpsertTransferOrder>> actualExample = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<UpsertTransferOrder>>() {
                });

        return actualExample.getBody();
    }

    private void createReportData(List<UpsertTransferOrder> result) {

        HashSet<Integer> storeCode = new HashSet<>();
        HashSet<Integer> article = new HashSet<>();
        totalArticleOrderQuantity = 0;

        for (UpsertTransferOrder upsertTransferOrder : result) {
            storeCode.add(upsertTransferOrder.getGoodsReceiverOutletId());
            article.add(upsertTransferOrder.getProductId());
            totalArticleOrderQuantity += upsertTransferOrder.getQuantity();
        }

        storeCount = storeCode.size();
        articleCount = article.size();
        totalLine = result.size();


//******************************************************************************************************************************************************


        List<UpsertTransferOrder> notValidUpserts = result.stream().filter(u -> (u.getIsValid() == 0)).collect(Collectors.toList());
        Map<Integer, List<UpsertTransferOrder>> productIdListUpsertMap = notValidUpserts.stream().collect(Collectors.groupingBy(UpsertTransferOrder::getProductId));

        // product id  , totalQuantity
        HashMap<Integer, Integer> artCountMap = new HashMap<>();
        // product id , stock
        HashMap<Integer, Integer> stockMap = new HashMap<>();


        for (Map.Entry<Integer, List<UpsertTransferOrder>> entry : productIdListUpsertMap.entrySet()) {
            Integer productId = entry.getKey();
            List<UpsertTransferOrder> upsertList = entry.getValue();
            Integer totalQauntity = upsertList.stream().mapToInt(UpsertTransferOrder::getQuantity).sum();
            artCountMap.put(productId, totalQauntity);
            stockMap.put(productId, upsertList.get(0).getArtStock());
        }


        insufficientStock = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : artCountMap.entrySet()) {

            Integer productId = entry.getKey();
            Integer totalQuantity = entry.getValue();
            Integer stock = stockMap.get(productId);

            if (stock == null || stock < totalQuantity) {
                HashMap<String, Integer> map = new HashMap<>();
                map.put("Product Id", productId);
                map.put("Total Order Quantity", totalQuantity);
                map.put("DWH Stock", stock == null ? 0 : stock);
                insufficientStock.add(map);
            }
        }
        createNotComposedOrdersReport(result);


    }

    private void createNotComposedOrdersReport(List<UpsertTransferOrder> result) {
        // *********************************************************************************************************************************************************************
        List<UpsertTransferOrder> notValidUpserts = result.stream().filter(u -> u.getIsValid() == 0).collect(Collectors.toList());

        Map<Integer, List<UpsertTransferOrder>> notCreatedOrdersMap = notValidUpserts.stream().collect(Collectors.groupingBy(UpsertTransferOrder::getGoodsReceiverOutletId));

        notComposedLines = new ArrayList<>();
        for (List<UpsertTransferOrder> notCreatedOrdersList : notCreatedOrdersMap.values()) {

            for (UpsertTransferOrder upsert : notCreatedOrdersList) {
                LinkedHashMap<String, String> map = new LinkedHashMap<>();
                map.put("Goods Receiver Outlet Id", Integer.toString(upsert.getGoodsReceiverOutletId()));
                map.put("Product Id", Integer.toString(upsert.getProductId()));
                map.put("Order Quantity", Integer.toString(upsert.getQuantity()));

                if (upsert.getSupplierNo() == null) {
                    map.put("Cause Of Error", "Goods Receiver Outlet id is not found.");
                } else {
                    map.put("Cause Of Error", "Insufficient Stock");
                }
                notComposedLines.add(map);
            }
        }
    }


    public void approveInvent() {

        logger.info("InventController.approveInvent() start for -> {}" + uploadedFile.getName());

        if (isExistErrorInReport()) {
            informationDialogMessage = "This file can not be uploaded.The replenishment file can not contains invalid order lines.";
            cancelInvent();
            PrimeFaces current = PrimeFaces.current();
            current.executeScript("PF('InformationDialog').show();");
            return;
        }

        int success = sendApproveInventFilePath();

        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('resultDialog').hide();");

        if (success == 1) {

            try {
                sendInventInfoMail(uploadedFile.getName());
            } catch (UnsupportedEncodingException e) {
                logger.info("Invent Info Mail could'nt send." + ExceptionUtils.getFullStackTrace(e));
            }

            PushTransferDTO pusTransferDTO = new PushTransferDTO();
            pusTransferDTO.setTransferAction(transferAction);
            pusTransferDTO.setFileName(uploadedFile.getName());

            HttpEntity<PushTransferDTO> response = startPushTransferService(pusTransferDTO);

            try {
                resultUploadedUpsertTransferOrders = response.getBody().getUpsertTransferOrderList();
                resultUploadedUpsertTransferOrderMessage = response.getBody().getMessage();
                PrimeFaces.current().executeScript("PF('requestResultDialog').show()");
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error on roboback response data", e.getMessage()));
            }


            FacesMessage message = new FacesMessage("Successfully", uploadedFile.getName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            logger.info("InventController.approveInvent() is success -> {}", success);

        } else if (success == 2) {
            FacesMessage message = new FacesMessage("Failed to load file",
                    "The Invent file already exists, and the Invent file is still in use by the push transfer service. Please try again in 10 minutes.");

            FacesContext.getCurrentInstance().addMessage(null, message);
            cancelInvent();

        } else {
            FacesMessage message = new FacesMessage("Failed to load file", "Please try again in 10 minutes.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            cancelInvent();
        }

    }

    private Integer sendApproveInventFilePath() {
        final String url = "http://localhost:9091/mediaSaturnController/approveInvent?filePath=" + uploadedFile.getPath();
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url, Integer.class);
    }

    private HttpEntity<PushTransferDTO> startPushTransferService(PushTransferDTO pushTransferDTO) {
        final String url = "http://localhost:9091/mediaSaturnController/startPushTransferService";
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(url, pushTransferDTO, PushTransferDTO.class);

    }

    public void cancelInvent() {

        logger.info("InventController.cancelInvent()");

        if (uploadedFile != null) {
            boolean isDeleted = uploadedFile.delete();
            logger.info("InventController.cancelInvent() end isDeleted {}", isDeleted);
        }

    }

    public Boolean sendInventInfoMail(String fileName) throws UnsupportedEncodingException {

        final String url = "http://localhost:9091/mediaSaturnController/sendInventInfoMail/";

        RestTemplate restTemplate = new RestTemplate();

        Map<String, String> uriVariables = new HashMap<>();

        uriVariables.put("articleCount", Integer.toString(articleCount));
        uriVariables.put("totalLine", Integer.toString(totalLine));
        uriVariables.put("storeCount", Integer.toString(storeCount));
        uriVariables.put("totalArticleOrderQuantity", Integer.toString(totalArticleOrderQuantity));
        uriVariables.put("fileName", fileName);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        uriVariables.put("loadDate", sdf.format(new Date()));

        return restTemplate.postForObject(url, uriVariables, Boolean.class);
    }

    public boolean isExistErrorInReport() {
        return !insufficientStock.isEmpty() || !notComposedLines.isEmpty();
    }


}
