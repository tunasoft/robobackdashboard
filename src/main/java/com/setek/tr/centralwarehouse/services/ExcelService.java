package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.model.StockExcelReportDTO;
import com.setek.tr.centralwarehouse.model.StockRobo;
import com.setek.tr.centralwarehouse.model.Transfer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ExcelService {

    @Autowired
    TransferDataService tranferService;
    @Autowired
    StockService stockService;

    public Workbook prepareExcelReportOfTheTransfers(List<Transfer> transfers, String headerType, String reportName) {
//        Workbook workbook = new HSSFWorkbook();
//        Sheet sheet = workbook.createSheet("Transfers");

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet(reportName);

        int baslikSayisi = baslikOlustur(sheet, headerType);
        int i = 1;

        Cell cell = null;
        for (Transfer transfer : transfers) {
            int baslikNo = 0;
            Row row = sheet.createRow(i);

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisNo());


            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getStoreCode());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getOutletId());


            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisTipi());

            if (headerType.equals("OpenOrders")) {
                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getStatus().toString());

                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getSiparisDurum());

            }
            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getArtNo());
            if (headerType.equals("OpenOrders")) {
                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getArtDurum());
            }

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisAdet());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisGirilen());
            if (headerType.equals("OpenOrders")) {
                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getSonDegistiren());

                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getOlusturmaTarihi().toString());
            }

            if (headerType.equals("GoodsIn")) {
                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getSaticiNo());
            }

            if (headerType.equals("GoodsOut") || headerType.equals("OpenOrders")) {
                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getSaticiAdi());
            }

            i++;
        }

        Row row = sheet.createRow(i);
        if (headerType.equals("OpenOrders")) {
            cell = row.createCell(6);

            cell.setCellValue(tranferService.calcultateSumQuantity("Quantity", transfers));

            cell = row.createCell(7);
            cell.setCellValue(tranferService.calcultateSumQuantity("Entered", transfers));
        } else {
            cell = row.createCell(3);

            cell.setCellValue(tranferService.calcultateSumQuantity("Quantity", transfers));

            cell = row.createCell(4);
            cell.setCellValue(tranferService.calcultateSumQuantity("Entered", transfers));
        }

        for (i = baslikSayisi; i >= 0; i--) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

    private int baslikOlustur(Sheet sheet, String headerType) {
        int baslikNo = 0;
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(baslikNo++);
        cell.setCellValue("Order No");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Store Code");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Outlet Id");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Order Type");

        if (headerType.equals("OpenOrders") || headerType.equals("All")) {
            cell = row.createCell(baslikNo++);
            cell.setCellValue("Status");

            cell = row.createCell(baslikNo++);
            cell.setCellValue("Order Condition");
        }

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Article No");

        if (headerType.equals("OpenOrders") || headerType.equals("All")) {
            cell = row.createCell(baslikNo++);
            cell.setCellValue("Art Condition");
        }

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Quantity of Order");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Order Entered");

        if (headerType.equals("OpenOrders") || headerType.equals("All")) {

            cell = row.createCell(baslikNo++);
            cell.setCellValue("Last Changer");
            cell = row.createCell(baslikNo++);
            cell.setCellValue("Date of Creation");

        }

        if (headerType.equals("GoodsIn") || headerType.equals("All")) {

            cell = row.createCell(baslikNo++);
            cell.setCellValue("Supplier No");

        }

        if (headerType.equals("All")) {
            cell = row.createCell(baslikNo++);
            cell.setCellValue("Delivery No");
        }

        if (!headerType.equals("GoodsIn")) {
            cell = row.createCell(baslikNo++);
            cell.setCellValue("Supplier Name");
        }

        if (headerType.equals("All")) {

            cell = row.createCell(baslikNo++);
            cell.setCellValue(
                    "The Shipment Date");

            cell = row.createCell(baslikNo++);
            cell.setCellValue(
                    "The ProcessDate");
        }

        return baslikNo;
    }


    public XSSFWorkbook wareHouse36Excel(List<StockExcelReportDTO> warehouse36ExcelDTOS) throws Exception {

        List<String> headers = getFieldNamesForClass(StockExcelReportDTO.class);

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Report Excel");

        int rowCount = 0;
        int columnCount = 0;

        Row row = sheet.createRow(0);


        for (String header : headers) {
            Cell cell = row.createCell(columnCount);
            cell.setCellValue(header);
            columnCount++;
        }

        sheet.setAutoFilter(new CellRangeAddress(0, 0, 0, columnCount - 1));

        rowCount++;
        Class<StockExcelReportDTO> classz = StockExcelReportDTO.class;
        for (StockExcelReportDTO excelDTO : warehouse36ExcelDTOS) {
            row = sheet.createRow(rowCount++);
            columnCount = 0;
            for (String fieldName : headers) {
                if (!fieldName.equals("serialVersionUID")) {
                    Cell cell = row.createCell(columnCount);
                    Method method = null;
                    try {
                        method = classz.getMethod("get" + capitalize(fieldName));
                    } catch (NoSuchMethodException nme) {
                        method = classz.getMethod("get" + fieldName);
                    }

                    Object value = method.invoke(excelDTO, (Object[]) null);
                    if (value != null) {
                        if (value instanceof String) {
                            cell.setCellValue((String) value);
                        } else if (value instanceof Long) {
                            cell.setCellValue((Long) value);
                        } else if (value instanceof Integer) {
                            cell.setCellValue((Integer) value);
                        } else if (value instanceof Double) {
                            cell.setCellValue((Double) value);
                        } else if (value instanceof Date) {
                            cell.setCellValue(value.toString());
                        }
                    }
                    columnCount++;
                }
            }
        }
        return workbook;
    }


    private static String capitalize(String s) {
        if (s.length() == 0)
            return s;
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    private static List<String> getFieldNamesForClass(Class<?> clazz) throws Exception {
        List<String> fieldNames = new ArrayList<String>();
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fieldNames.add(fields[i].getName());
        }
        return fieldNames;
    }


    public Workbook prepareExcelReportOfTheStocks(List<StockRobo> stockRobos) {

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Stock");

        int baslikSayisi = baslikOlustur2(sheet);
        int i = 1;

        Cell cell = null;
        for (StockRobo stock : stockRobos) {
            int baslikNo = 0;
            Row row = sheet.createRow(i);

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getArticleCode());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getEkolStock());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getDwhStock());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getStockInPlanned() != null ? stock.getStockInPlanned() : 0);

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getStockOutPlanned() != null ? stock.getStockOutPlanned() : 0);

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getEkolExpectedStock() != null ? stock.getEkolExpectedStock() : 0);

            cell = row.createCell(baslikNo++);
            cell.setCellValue(stock.getControlDate().toString());

            i++;
        }
        Row row = sheet.createRow(i);
        cell = row.createCell(1);

        cell.setCellValue(stockService.calcultateSumQuantity("EkolStock", stockRobos));

        cell = row.createCell(2);
        cell.setCellValue(stockService.calcultateSumQuantity("DwhStock", stockRobos));

        for (i = baslikSayisi; i >= 0; i--) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

    private int baslikOlustur2(Sheet sheet) {
        int baslikNo = 0;
        Row row = sheet.createRow(0);

        Cell cell = row.createCell(baslikNo++);
        cell.setCellValue("Article Code");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Ekol Stock");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("DWH Stock");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Stock In Planned");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Stock Out Planned");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Ekol Expected Stock");

        cell = row.createCell(baslikNo++);
        cell.setCellValue("Control Date");

        return baslikNo;
    }

    public Workbook prepareExcelReportOfTransfersInSomeInterval(List<Transfer> transfers) {
//        Workbook workbook = new HSSFWorkbook();
//        Sheet sheet = workbook.createSheet("Transfers");

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Transfers");

        int baslikSayisi = baslikOlustur(sheet, "All");
        int i = 1;

        Cell cell = null;
        for (Transfer transfer : transfers) {
            int baslikNo = 0;
            Row row = sheet.createRow(i);

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisNo());


            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getStoreCode());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getOutletId());


            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisTipi());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getStatus().toString());
            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisDurum());
            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getArtNo());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getArtDurum());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisAdet());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSiparisGirilen());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSonDegistiren());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getOlusturmaTarihi().toString());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSaticiNo());
            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getIrsaliyeNo());

            cell = row.createCell(baslikNo++);
            cell.setCellValue(transfer.getSaticiAdi());

            if (transfer.getSevkTarihi() != null) {
                cell = row.createCell(baslikNo++);

                cell.setCellValue(transfer.getSevkTarihi().toString());
            } else {
                cell = row.createCell(baslikNo++);
                cell.setCellValue("");
            }

            if (transfer.getProcessDate() != null) {
                cell = row.createCell(baslikNo++);
                cell.setCellValue(transfer.getProcessDate().toString());
            }
            i++;
        }

        Row row = sheet.createRow(i);
        cell = row.createCell(6);

        cell.setCellValue(tranferService.calculateAllSumQuantityInSomeInterval("Quantity", transfers));

        cell = row.createCell(7);
        cell.setCellValue(tranferService.calculateAllSumQuantityInSomeInterval("Entered", transfers));

        for (i = baslikSayisi; i >= 0; i--) {
            sheet.autoSizeColumn(i);
        }
        return workbook;
    }

}
