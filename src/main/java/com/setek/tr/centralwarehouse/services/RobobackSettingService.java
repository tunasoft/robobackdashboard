package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.RestClients.RobobackResponse;
import com.setek.tr.centralwarehouse.constant.ServiceName;
import com.setek.tr.centralwarehouse.model.MailSettingDTO;
import com.setek.tr.centralwarehouse.model.RobobackSettings;
import com.setek.tr.centralwarehouse.model.SettingsReturn;
import com.setek.tr.centralwarehouse.persistance.RobobackSettingsRepo;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RobobackSettingService {


    private final Logger logger = LoggerFactory.getLogger(RobobackSettingService.class);

    private final RobobackSettingsRepo robobackSettingsRepo;

    private static final String SETTING_BASE_URL = "http://localhost:9091/settings/";


    public List<MailSettingDTO> getMailSettings() {
        RestTemplate template = new RestTemplate();

        MailSettingDTO[] result = template.getForObject(SETTING_BASE_URL + "mail-settings-get", MailSettingDTO[].class);

        assert result != null;
        return Arrays.asList(result);
    }

    public RobobackResponse saveMailSettings(List<MailSettingDTO> settingList) {
        RestTemplate template = new RestTemplate();

        return template.postForObject(SETTING_BASE_URL + "mail-settings-save", settingList, RobobackResponse.class);
    }


    public void triggerService(FacesContext context, ServiceName serviceName) {
        String url = "http://localhost:9091/settings/trigger-service";
        RestTemplate temp = new RestTemplate();

        String response;

        try {
            logger.info("Starting triggerService() -> {}", serviceName);
            response = temp.postForObject(url, serviceName, String.class);
        } catch (Exception e) {
            logger.error("triggerService() fail -> " + serviceName, e);
            response = e.getLocalizedMessage();
        }
        context.addMessage(null, new FacesMessage(serviceName.getDisplayName(), response));
    }


    public List<RobobackSettings> findAll() {
        return robobackSettingsRepo.findAll();
    }

    public int[] setValid() {
        int[] valid = new int[4];
        for (int i = 0; i < 4; i++) {
            valid[i] = 1;
        }
        return valid;
    }

    public SettingsReturn save(List<RobobackSettings> robobackSettings) {

        List<String> errors = new ArrayList<>();
        SettingsReturn settingsReturn = new SettingsReturn();
        settingsReturn.setFlag(true);
        settingsReturn.setErrors(errors);

        for (RobobackSettings robobackSetting : robobackSettings) {
            int[] valid = setValid();

            if (robobackSetting.isDaily()) {
                if (!checkDaily(robobackSetting)) {
                    valid[0] = 0;
                    settingsReturn.getErrors().add(robobackSetting.getServiceName() + " -> Daily: please check the 'hour' and 'minute' fields ");
                }
            }
            if (robobackSetting.isHourly()) {
                if (!checkHourly(robobackSetting)) {
                    valid[1] = 0;
                    settingsReturn.getErrors().add(robobackSetting.getServiceName() + " -> Hourly: Please check the 'minute' field");
                }
            }
            if (robobackSetting.isPeriodically()) {
                if (!checkPeriodically(robobackSetting)) {
                    valid[2] = 0;
                    settingsReturn.getErrors().add(robobackSetting.getServiceName() + " -> Periodically: Please check the 'minute' field");
                }
            }
            if (robobackSetting.isWorkBySpecifiedTime()) {
                if (!checkWorkBySpecifiedTime(robobackSetting)) {
                    valid[3] = 0;
                    settingsReturn.getErrors().add(robobackSetting.getServiceName() + " -> Specified Time: Please check the 'working hours' field. Field must be '19:12,10:20'");
                }
            }
            if (checkValid(valid)) {
                robobackSettingsRepo.save(robobackSetting);
            } else {
                settingsReturn.setFlag(false);
            }

        }

        RestTemplate template = new RestTemplate();

        if (settingsReturn.isFlag()) {
            String reloadInfo = template.getForObject(SETTING_BASE_URL + "reloadSettings", String.class);
            logger.info("Settings Reload -> {}", reloadInfo);
        }

        return settingsReturn;

    }

    public boolean checkValid(int[] valid) {

        return valid[0] == 1 && valid[1] == 1 && valid[2] == 1 && valid[3] == 1;
    }

    public boolean checkDaily(RobobackSettings robobackSetting) {
        return robobackSetting.getHour() >= 0 && robobackSetting.getHour() < 24 && robobackSetting.getMinute() < 60
                && robobackSetting.getMinute() >= 0;
    }

    public boolean checkHourly(RobobackSettings robobackSetting) {
        return robobackSetting.getMinute() < 60 && robobackSetting.getMinute() >= 0;

    }

    public boolean checkPeriodically(RobobackSettings robobackSetting) {
        return robobackSetting.getMinute() <= 24 * 60 && robobackSetting.getMinute() > 0;
    }

    public boolean checkWorkBySpecifiedTime(RobobackSettings robobackSetting) {
        return !robobackSetting.getWorkingHours().equals("0") && checkFormat(robobackSetting);

    }

    public boolean checkFormat(RobobackSettings robobackSetting) {
        try {
            String workingHours = robobackSetting.getWorkingHours();
            String[] splitedWorkingHours = workingHours.split(",");
            int x = count(workingHours, ',');
            if (splitedWorkingHours.length != x + 1 && workingHours.contains(",")) {
                return false;
            }
            for (int i = 0; i < splitedWorkingHours.length; i++) {

                String[] splitedParams = splitedWorkingHours[i].split(":");
                if (!(Integer.parseInt(splitedParams[0]) < 24 && Integer.parseInt(splitedParams[1]) < 60
                        && splitedParams.length == 2 && splitedParams[0].length() == 2
                        && splitedParams[1].length() == 2)) {
                    return false;
                }
            }

            return true;

        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return false;

        }
    }

    int count(String workingHours, char c) {
        // Count variable
        int res = 0;

        for (int i = 0; i < workingHours.length(); i++)
            if (workingHours.chars().toArray()[i] == c)
                res++;

        return res;
    }

}
