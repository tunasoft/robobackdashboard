/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.setek.tr.centralwarehouse.model.StockRobo;
import com.setek.tr.centralwarehouse.persistance.StockRepository;

/**
 *
 * @author ASUS
 */
@Service
public class StockService {

	@Autowired
	StockRepository stockRepository;
//
//	@Autowired
//	EmailService emailService;


	public List<StockRobo> getStocksToday() {

		List<StockRobo> stocks = new ArrayList<StockRobo>();
		
		SimpleDateFormat sdfToday = new SimpleDateFormat("yyyy-MM-dd");
		
		Calendar beforeToday = Calendar.getInstance();
		try {
			beforeToday.setTime(sdfToday.parse(sdfToday.format(new Date())));
			
			beforeToday.add(Calendar.DAY_OF_MONTH, -1);
						
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
			
			String yesterday = sdf.format(beforeToday.getTime());
			
			stocks = stockRepository.findByControlDateStocks(yesterday) ; 

			return stocks;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return stocks;
	}
	
	
	public List<StockRobo> getStocksTimeBetween(Date startDate, Date endDate , int first, int pageSize, String sortField, SortOrder sortOrder) {
		
		return stockRepository.getStocksTimeBetween(startDate, endDate, first, pageSize, sortField, sortOrder);
	}
	
	
	public List<StockRobo> retrieveStocksByParam(Map<String, Object> paramMaps, int first, int pageSize, String sortField, SortOrder sortOrder) {
		return stockRepository.retrieveStocksByParam(paramMaps, first, pageSize, sortField, sortOrder);
	}

	
	public List<StockRobo> retrieveStocksByParamForExcel(Map<String, Object> paramMaps){
		return stockRepository.retrieveStocksByParamForExcel(paramMaps);
	}

	public Long retrieveStocksCountByParam(Map<String, Object> paramMaps) {
		return stockRepository.retrieveStocksCountByParam(paramMaps);
	}


	public Long getStocksTimeBetweenCount(Date startDate, Date endDate) {
		return stockRepository.getStockTimeBetweenCount(startDate, endDate);	
	}
	public List<StockRobo>  getStocksTimeBetween(Date startDate , Date endDate ){
		return stockRepository.getStocksTimeBetween(startDate, endDate);
	}
	public long calcultateSumQuantity(String columnName, List<StockRobo> stocks) {
		long sum = 0;
		System.out.println("StockService.calcultateSumQuantity() column " + columnName);
		if (columnName.equals("EkolStock")) {
			
			for (StockRobo stock : stocks) {
				sum = sum + stock.getEkolStock();
			}
		} else {
			for (StockRobo stock : stocks) {
				sum = sum + stock.getDwhStock();
			}
		}
		
		return sum;
	}

}

