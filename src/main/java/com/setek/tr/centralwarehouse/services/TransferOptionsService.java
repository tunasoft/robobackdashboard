/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.RestClients.RobobackClient;
import com.setek.tr.centralwarehouse.RestClients.RobobackResponse;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.model.TransferOperationLog;
import com.setek.tr.centralwarehouse.persistance.TransferOperationLogRepo;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author ASUS
 */
@Service
@Getter
@Setter
public abstract class TransferOptionsService {


    @Autowired
    private RobobackClient robobackClient;

    @Autowired
    private TransferDataService transferDataService;

    @Autowired
    private TransferOperationLogRepo transferOperationLogRepo;

    private final Logger log = LoggerFactory.getLogger(TransferOptionsService.class);

    @Transactional
    public abstract void openDialog(Transfer transfer);

    public abstract void closeDialog();

    public abstract void startProcess();

    protected abstract boolean isAvailableForProcess(Integer status);


    void saveCancelOperationLog(TransferOperationLog log) {
        transferOperationLogRepo.save(log);
    }


    protected TransferOperationLog createOperationLog(RobobackResponse response, String orderNo, String orderType) {
        TransferOperationLog col = new TransferOperationLog();
        col.setRequestDate(new Date());
        col.setTransferNo(orderNo);
        col.setTransferType(orderType);
        col.setRqMessage(response.getMessages());

        String transactionCode = "";
        //response.getResponseData().get("transactionCode") != null ? response.getResponseData().get("transactionCode").toString() : "";
        col.setRqTransactionCode(transactionCode);
        return col;
    }

   /*public  boolean isTransferAvailableForProcess(Transfer selectedTransfer) {

        System.out.println("com.setek.tr.CentrolWareHouse.controller.TransferOptionsService.isTransferAvailableForProcess()");
        Boolean isServiceWorking = robobackClient.isReturnDataServiceWorking();

        if (!isServiceWorking) {

            RobobackResponse response = robobackClient.getCurrentStatusOfTransferFromEkol(selectedTransfer);

            if (response.isSuccess()) {
                Integer ekolStatus = selectedTransfer.getEkolStatus();

                if (!response.getMessages().equals("There is no updatedTransaction")) {
                    ekolStatus = (Integer) response.getResponseData().get("ekolStatus");
                }

                if (isStatuAvailableForProcces(ekolStatus)) {
                    return true;
                } else {
                    FacesMessage message = new FacesMessage("UnSaved",
                            selectedTransfer.getSiparisNo() + "Can not be cancel or edit. Status is changed by Ekol.");

                    FacesContext.getCurrentInstance().addMessage(null, message);
                }

            } else {

                FacesMessage message = new FacesMessage("UnSaved",
                        selectedTransfer.getSiparisNo() + " -> " + response.getMessages());
                FacesContext.getCurrentInstance().addMessage(null, message);
            }

        } else { // data güncelleme servisi  çalıştığı için cancel işlemini yapma.
            FacesMessage message = new FacesMessage("UnSaved",
                    selectedTransfer.getSiparisNo() + " : Transfer update service is working now. Please try after 5 minutes.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return false;
    }*/

   /* private LinkedHashMap<String, List<String>> prepareCancelMapForRobot(List<Transfer> transfers) {
        log.info("com.setek.tr.CentrolWareHouse.controller.TransferOptionsService.prepareCancelMapForRobot()");
        LinkedHashMap<String, List<String>> cancelMapForRobot = new LinkedHashMap<>();

        for (Transfer transfer : transfers) {

            if (cancelMapForRobot.containsKey(transfer.getSiparisNo())) {
                cancelMapForRobot.get(transfer.getSiparisNo()).add(Integer.toString(transfer.getArtNo()));
            } else {
                List<String> list = new ArrayList<>();
                list.add(Integer.toString(transfer.getArtNo()));
                cancelMapForRobot.put(transfer.getSiparisNo(), list);
            }
        }
        return cancelMapForRobot;
    }
    	public boolean isSuitableForSave() {
		System.out.println("TransferController.isSuitableForSave()");
		if ((transfersForEdit == null || transfersForEdit.isEmpty()) && isEditedRow == false ) {
			return true;
		}
		return false;
	}
       private List<Transfer> setCancelRequestStatus(String orderNo) {
        System.out.println("com.setek.tr.CentrolWareHouse.controller.TransferController.setCancelRequestStatus()");
        List<Transfer> cancelList = transferDataService.findTransfersBySiparisNo(orderNo);

        for (Transfer transfer : cancelList) {
            transfer.setStatus(Status.CancelRequest);
        }
        return cancelList;
    }
    // -------------- preparing cancel data for robot --------------------------
		LinkedHashMap<String, List<String>> cancelMapForRobot = prepareCancelMapForRobot(transfersForEdit);

		// ----------------------------- send cancel data to robot
		try {
			boolean result = mrRobotClient.sendCancelDataToRobot(cancelMapForRobot);

			if (!result) {
				FacesMessage mess = new FacesMessage("Robotic", "Could not connect to robitc service.");
				FacesContext.getCurrentInstance().addMessage(null, mess);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}*/
}
