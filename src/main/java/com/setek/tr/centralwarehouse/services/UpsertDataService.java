package com.setek.tr.centralwarehouse.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.setek.tr.centralwarehouse.model.UpsertTransferOrder;
import com.setek.tr.centralwarehouse.persistance.UpsertTransferOrderRepository;

@Service
public class UpsertDataService {

	@Autowired
	private UpsertTransferOrderRepository upsertRepo;

	private final Logger log = LoggerFactory.getLogger(UpsertTransferOrderRepository.class);

	public List<UpsertTransferOrder> getTodayLoadedUpserts(String today,String tomorrow) {
		try {
			return upsertRepo.getTodayLoadedUpserts(today,tomorrow);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return new ArrayList<>();
	}

}
