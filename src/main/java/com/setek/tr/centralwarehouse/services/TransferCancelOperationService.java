/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.RestClients.RobobackResponse;
import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.model.TransferOperationLog;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ASUS
 */
@Getter
@Setter
@Service
public class TransferCancelOperationService extends TransferOptionsService {

    private final Logger log = LoggerFactory.getLogger(TransferCancelOperationService.class);

    private List<Transfer> transfersForCancel;

    private Transfer selectedTransfer;

    /**
     * @param transfer
     */
    @Transactional
    @Override
    public void openDialog(Transfer transfer) {
        this.selectedTransfer = transfer;
        this.transfersForCancel = getTransferDataService().findTransfersBySiparisNo(transfer.getSiparisNo());
    }

    @Override
    public void startProcess() {


        log.info("TransferCancelOperationService.startProcess");

        RobobackResponse operationDataCancelRequestResponse = sendCancelRequestForOperationData();

        RobobackResponse ekolCancelRequest = sendCancelRequestForEkol();

        if (!ekolCancelRequest.isSuccess()) {
            FacesMessage message = new FacesMessage("Failed", selectedTransfer.getSiparisNo() + "Selected articles couldn't cancel." + ekolCancelRequest.getMessages());
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            FacesMessage message = new FacesMessage("Success", "Cancellation Ekol step successfully.");
            FacesContext.getCurrentInstance().addMessage(null, message);

        }

        if (ekolCancelRequest.isSuccess()) {

            transfersForCancel.forEach(t -> t.setStatus(Status.Cancel));
            getTransferDataService().saveTransfers(transfersForCancel);
            FacesMessage message = new FacesMessage("Successful", selectedTransfer.getSiparisNo() + " -> " + " Selected articles cancelled.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            FacesMessage message = new FacesMessage("Failed", selectedTransfer.getSiparisNo() + "Selected articles couldn't cancel." + ekolCancelRequest.getMessages());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

        TransferOperationLog opLog = createOperationLog(ekolCancelRequest, selectedTransfer.getSiparisNo(), selectedTransfer.getSiparisTipi());
        saveCancelOperationLog(opLog);


        closeDialog();
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('orderCancelDialog').hide();");

    }

    private RobobackResponse sendCancelRequestForEkol() {

        RobobackResponse response;
        response = getRobobackClient().sendCancelRequestForLines(selectedTransfer.getSiparisNo(), transfersForCancel.stream().map(e -> String.valueOf(e.getArtNo())).collect(Collectors.toList()));
        return response;
    }

    @Override
    protected boolean isAvailableForProcess(Integer status) {
        if (status == null) {
            return false;
        }
        return (status < 40);
    }

    private RobobackResponse sendCancelRequestForOperationData() {
        return getRobobackClient().sendCancelRequestForAllOrder(selectedTransfer.getSiparisNo());
    }


    @Override
    public void closeDialog() {
        transfersForCancel = null;
        selectedTransfer = null;
    }

}
