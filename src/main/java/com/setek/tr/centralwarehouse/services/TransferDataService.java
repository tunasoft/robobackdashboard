/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.model.Transfer;
import com.setek.tr.centralwarehouse.persistance.TransferRepository;
import lombok.Getter;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ASUS
 */
@Service
public class TransferDataService {

	private Logger log = LoggerFactory.getLogger(TransferDataService.class);

	@Autowired
        @Getter
	private TransferRepository transferRepository;

	public void deleteTransfers(List<Transfer> transfers) {
		transferRepository.deleteAll(transfers);

	}

	public List<Transfer> getTransfersTimeBetween(Date startDate, Date endDate) {

		log.info("TransferService.getTransfers() start date + " + startDate + "  end date " + endDate);

		// 2019-04-16 00:00:00
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String sStartDate = sdf.format(startDate);
		String sEndDate = sdf.format(endDate);
          */
		List<Transfer> transfers = transferRepository.findAllByOlusturmaTarihiBetween(startDate, endDate);

		log.info("TransferService.getTransfers() end");


        return transfers;
    }

    public List<Transfer> retrieveTransfersByParam(Map<String, Object> paramMaps, int first, int pageSize,
                                                   String sortField, SortOrder sortOrder, Map<String, String> filterProps) {
        return transferRepository.retrieveTransfersByParam(paramMaps, first, pageSize, sortField, sortOrder, filterProps);
    }

    public Long retrieveTransfersCountByParam(Map<String, Object> paramMaps, Map<String, String> filterProps) {
        return transferRepository.retrieveTransfersCountByParam(paramMaps, filterProps);
    }

    public List<Transfer> getTransfersTimeBetween(Date startDate, Date endDate, int first, int pageSize,
                                                  String sortField, SortOrder sortOrder) {

        return transferRepository.getTransfersTimeBetween(startDate, endDate, first, pageSize, sortField, sortOrder);
    }

    public Long getTransfersTimeBetweenCount(Date startDate, Date endDate) {

		return transferRepository.getTransfersTimeBetweenCount(startDate, endDate);
	}

	public List<Transfer> retrieveOpenOrdersByParam(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> paramMaps) {
		return transferRepository.retrieveOpenOrdersByParam(first, pageSize, sortField, sortOrder, paramMaps);
	}

	public List<Transfer> retrieveOpenOrdersByParamForExcel(Map<String, Object> paramMaps) {
		return transferRepository.retrieveOpenOrdersByParamForExcel(paramMaps);
	}

	public Long retrieveOpenOrdersByParamCount(Map<String, Object> paramMaps) {
		return transferRepository.retrieveOpenOrdersByParamCount(paramMaps);
	}

	public List<Transfer> retrieveOpenOrders(int first, int pageSize, String sortField, SortOrder sortOrder) {
		return transferRepository.retrieveOpenOrders(first, pageSize, sortField, sortOrder);
	}

	public Long retriveOpenOrdersCount() {
		return transferRepository.retrieveOpenOrdersCount();
	}

	public List<Transfer> getGoodsOutAcToStatus() {

		return transferRepository.getGoodsOutAcToStatus();
	}

	public List<Transfer> getGoodsInAcToStatus() {

		return transferRepository.getGoodsInAcToStatus();
	}

	public List<Transfer> getGoodsOutAcToStatus2(Date date) {

		return transferRepository.getGoodsOutAcToStatus2(date);
	}

	public List<Transfer> getGoodsInAcToStatus2(Date date) {

		return transferRepository.getGoodsInAcToStatus2(date);
	}

	public List<Transfer> retrieveGoodsOutDailyTransfer(int first, int pageSize, String sortField,
			SortOrder sortOrder) {
		return transferRepository.retrieveGoodsOutDailyTransfer(first, pageSize, sortField, sortOrder);
	}

	public Long retrieveGoodsOutDailyTransferCount() {
		return transferRepository.retrieveGoodsOutDailyTransferCount();
	}

	public List<Transfer> retrieveGoodsInDailyTransfer(int first, int pageSize, String sortField, SortOrder sortOrder) {
		return transferRepository.retrieveGoodsInDailyTransfer(first, pageSize, sortField, sortOrder);
	}

	public Long retrieveGoodsInDailyCount() {
		return transferRepository.retrieveGoodsInDailyCount();
	}

	public List<Transfer> retrieveGoodsOutDailyTransferForFilter(Map<String, Object> paramterMaps, int first,
			int pageSize, String sortField, SortOrder sortOrder) {
		return transferRepository.retrieveGoodsOutDailyTransferForFilter(paramterMaps, first, pageSize, sortField,
				sortOrder);
	}

	public Long retrieveGoodsOutDailyTransferCountForFilter(Map<String, Object> paramterMaps) {
		return transferRepository.retrieveGoodsOutDailyTransferCountForFilter(paramterMaps);
	}

	public List<Transfer> retrieveGoodsInDailyTransferForFilter(Map<String, Object> paramterMaps, int first,
			int pageSize, String sortField, SortOrder sortOrder) {
		return transferRepository.retrieveGoodsInDailyTransferForFilter(paramterMaps, first, pageSize, sortField,
				sortOrder);
	}

	public List<Transfer> retrieveGoodsInPastTransferForFilter(Map<String, Object> paramterMaps, int first,
			int pageSize, String sortField, SortOrder sortOrder, Date date) {
		return transferRepository.retrieveGoodsInPastTransferForFilter(paramterMaps, first, pageSize, sortField,
				sortOrder, date);
	}

	public Long retrieveGoodsInDailyTransferCountForFilter(Map<String, Object> paramterMaps) {
		return transferRepository.retrieveGoodsInDailyTransferCountForFilter(paramterMaps);
	}

	public long calcultateSumQuantity(String columnName, List<Transfer> transferList) {
		long sum = 0;

		if (columnName.equals("Quantity")) {

			for (Transfer transfer : transferList) {
				sum = sum + transfer.getSiparisAdet();
			}
		} else {
			for (Transfer transfer : transferList) {
				sum = sum + transfer.getSiparisGirilen();
			}
		}

		return sum;
	}

	public long calculateAllSumQuantityInSomeInterval(String columnName, List<Transfer> transferList) {
		long sum = 0;
		if (columnName.equals("Quantity")) {
			for (Transfer transfer : transferList) {
				sum = sum + transfer.getSiparisAdet();
			}
		} else {
			for (Transfer transfer : transferList) {
				sum = sum + transfer.getSiparisGirilen();
			}
		}

		return sum;

	}

	public List<Transfer> retrieveGoodsInPastTransfer(int first, int pageSize, String sortField, SortOrder sortOrder,
			Date date) {
		return transferRepository.retrieveGoodsInPastTransfer(first, pageSize, sortField, sortOrder, date);
	}

	public Long retrieveGoodsInPastCount(Date date) {
		return transferRepository.retrieveGoodsInPastCount(date);

	}

	public Long retrieveGoodsInPastTransferCountForFilter(Map<String, Object> paramterMaps, Date date) {
		return transferRepository.retrieveGoodsInPastTransferCountForFilter(paramterMaps, date);
	}

	public List<Transfer> retrieveGoodsOutPastTransfer(int first, int pageSize, String sortField, SortOrder sortOrder,
			Date date) {
		return transferRepository.retrieveGoodsOutPastTransfer(first, pageSize, sortField, sortOrder, date);

	}

	public Long retrieveGoodsOutPastTransferCount(Date date) {
		return transferRepository.retrieveGoodsOutPastTransferCount(date);
	}

	public List<Transfer> retrieveGoodsOutPastTransferForFilter(Map<String, Object> paramterMaps, int first,
			int pageSize, String sortField, SortOrder sortOrder, Date date) {
		return transferRepository.retrieveGoodsOutPastTransferForFilter(paramterMaps, first, pageSize, sortField,
				sortOrder, date);
	}

	public Long retrieveGoodsOutPastTransferCountForFilter(Map<String, Object> paramterMaps, Date date) {
		return transferRepository.retrieveGoodsOutPastTransferCountForFilter(paramterMaps, date);
	}

	public List<Transfer> retrieveOpenOrders() {
		return transferRepository.retrieveOpenOrders();
	}

	public List<Transfer> getTransfersTimeBetween2(Date startDate, Date endDate) {
		return transferRepository.getTransfersTimeBetween2(startDate, endDate);
	}

	public Long retrieveTransferCountStatus4ForFilter(Map<String, Object> paramterMaps) {
		return transferRepository.retrieveTransferCountStatus4ForFilter(paramterMaps);

	}

	public List<Transfer> retrieveStatus4ForFilter(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder) {
		return transferRepository.retrieveStatus4ForFilter(paramterMaps, first, pageSize, sortField, sortOrder);
	}

	public Long retrieveTransfersStatus4Count() {

		return transferRepository.retrieveTransfersStatus4Count();
    }

    public List<Transfer> retrieveTransfersStatus4(int first, int pageSize, String sortField, SortOrder sortOrder) {
        return transferRepository.retrieveTransfersStatus4(first, pageSize, sortField, sortOrder);
    }

    public List<Transfer> getTransferByExternalOrderId(HashSet<String> externalIds) {
        return transferRepository.getTransferByExternalOrderId(externalIds);
    }

    public List<Transfer> retrieveTransfersByParamforExcel(Map<String, Object> paramterMaps, Map<String, String> filterProps) {
        return transferRepository.retrieveTransfersByParamforExcel(paramterMaps, filterProps);
    }

    public List<Transfer> retrieveGoodsInPastTransferForFilterForExcel(Map<String, Object> paramterMaps, Date date) {
        return transferRepository.retrieveGoodsInPastTransferForFilterForExcel(paramterMaps, date);
    }

    public List<Transfer> retrieveGoodsOutPastTransferForFilterForExcel(Map<String, Object> paramterMaps, Date date) {
        return transferRepository.retrieveGoodsOutPastTransferForFilterForExcel(paramterMaps, date);
    }

    public List<Transfer> findTransfersBySiparisNo(String siparisNo) {
        return transferRepository.getTransferBySiparisNo(siparisNo);
    }

    public boolean isExistTransferBySiparisNo(String siparisNo) {
        return transferRepository.existsBySiparisNo(siparisNo);
    }

    public List<Transfer> saveTransfers(List<Transfer> transfers) {
        return transferRepository.saveAll(transfers);
    }
}
