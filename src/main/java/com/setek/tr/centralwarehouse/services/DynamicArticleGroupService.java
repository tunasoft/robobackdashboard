package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.model.DynamicArticleGroup;
import com.setek.tr.centralwarehouse.persistance.DynamicArticleGroupRepository;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DynamicArticleGroupService {

    private final DynamicArticleGroupRepository dynamicArticleGroupRepository;

    private final Logger log = LoggerFactory.getLogger(DynamicArticleGroupService.class);

    public DynamicArticleGroupService(DynamicArticleGroupRepository dynamicArticleGroupRepository) {
        this.dynamicArticleGroupRepository = dynamicArticleGroupRepository;
    }


    public List<DynamicArticleGroup> retrieveTransfersByParam(Map<String, Object> paramMaps, int first, int pageSize,
                                                              String sortField, SortOrder sortOrder) {
        return dynamicArticleGroupRepository.retrieveArticelsByParams(paramMaps, first, pageSize, sortField, sortOrder);
    }

    public Long retrieveTotalDataCount() {
        return dynamicArticleGroupRepository.count();
    }

    public void saveArticle(DynamicArticleGroup dynamicArticleGroup) {
        dynamicArticleGroupRepository.save(dynamicArticleGroup);
    }


}
