package com.setek.tr.centralwarehouse.services;


import com.setek.tr.centralwarehouse.model.ArticleGroupName;
import com.setek.tr.centralwarehouse.persistance.ArticleGroupNameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ArticleGroupNameService {

    private final ArticleGroupNameRepository articleGroupNameRepository;

    private List<ArticleGroupName> articleGroupNames = null;

    public List<ArticleGroupName> fetchAllArticleGroupNames() {

        if (articleGroupNames == null || articleGroupNames.isEmpty()) {
            articleGroupNames = articleGroupNameRepository.findAll();
        }

        return articleGroupNames;
    }

    public void refreshList() {
        articleGroupNames = articleGroupNameRepository.findAll();
    }

    public void saveArticleGroups(List<ArticleGroupName> articleGroupNames) {
        articleGroupNameRepository.saveAll(articleGroupNames);
    }

    public void compareAndSaveArticleGroups(List<ArticleGroupName> articleGroupNames) {
        refreshList();

        for (ArticleGroupName saveAbleArticleGroupName : articleGroupNames) {

            boolean isExist = false;
            for (ArticleGroupName savedArticleGroupName : articleGroupNames)
                if (savedArticleGroupName.getGroupCode().equals(saveAbleArticleGroupName.getGroupCode())) {
                    isExist = true;
                    break;
                }

            if (!isExist) {
                articleGroupNameRepository.save(saveAbleArticleGroupName);
            }
        }
    }


    public void saveArticleGroup(ArticleGroupName object) {

        if (object.getGroupType() == null || object.getGroupType().isEmpty()) {
            object.setGroupType("OTHER");
        }
        articleGroupNameRepository.save(object);
    }
}
