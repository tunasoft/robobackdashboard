package com.setek.tr.centralwarehouse.services;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.setek.tr.centralwarehouse.model.ErrorLogs;
import com.setek.tr.centralwarehouse.persistance.ErrorLogsRepository;

@Service
public class ErrorLogsService {

    @Autowired
    ErrorLogsRepository errorLogsRepository;

    public List<ErrorLogs> retrieveLogsByParam(Map<String, Object> paramterMaps, int first, int pageSize, String sortField,
            SortOrder sortOrder) {
        return errorLogsRepository.retrieveLogsByParam(paramterMaps, first, pageSize, sortField, sortOrder);
    }

    public Long retrieveLogsCountByParam(Map<String, Object> paramterMaps) {
        return errorLogsRepository.retrieveLogsCountByParam(paramterMaps);
    }

    public Long retrieveLogsCount() {
        return errorLogsRepository.retrieveLogsCount();

    }

    public List<ErrorLogs> retrieveLogs(int first, int pageSize,
            String sortField, SortOrder sortOrder) {

        return errorLogsRepository.retrieveLogs(first, pageSize, sortField, sortOrder);
    }

}
