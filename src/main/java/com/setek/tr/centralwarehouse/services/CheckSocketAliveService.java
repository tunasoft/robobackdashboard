package com.setek.tr.centralwarehouse.services;

import com.setek.tr.centralwarehouse.ServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

@Service
public class CheckSocketAliveService implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(CheckSocketAliveService.class);

    public boolean checkSocketForServiceIsAlive(ServiceInfo service){

        if (log.isDebugEnabled()){
            log.info("checkSocketForServiceIsAlive  start for ->" + service.toString());
        }

        boolean isAlive = false;

        //Creates a socket address from a ServiceInfo hostname and port number
        SocketAddress socketAddress = new InetSocketAddress(service.getHostName(),service.getPort());
        Socket socket = new Socket();


        //Timeout reuired for this process
        int timeOut = 2500;
        if (log.isDebugEnabled()){
            log.debug(String.format("checkSocketForServiceIsAlive - > trying connect for %s service", service.toString()));
        }


        try {
            socket.connect(socketAddress,timeOut);
            isAlive = socket.isConnected();
        } catch (SocketTimeoutException exception) {
            log.info("SocketTimeoutException " + service.getHostName() + ":" + service.getPort() + ". " + exception.getMessage());
        } catch (IOException exception) {
            log.info("IOException - Unable to connect to " +service.getHostName() + ":" + service.getPort() +  ". " + exception.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                log.info("Socked close fail for -> " + service.toString());
            }
        }
      return isAlive;
    }


}
