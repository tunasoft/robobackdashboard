/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.setek.tr.centralwarehouse.model.StockRobo;

/**
 *
 * @author ASUS
 */
@Repository
public interface StockRepository extends JpaRepository<StockRobo, Long>  , StockRepositoryCustom {

	
    @Query(value = "SELECT * FROM dbo.stockrobo WHERE control_date >= ?1", nativeQuery = true)
    List<StockRobo> findByControlDateStocks(String date);
	

}
