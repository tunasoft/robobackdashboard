/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.TransferOperationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASUS
 */
@Repository
public interface TransferOperationLogRepo extends JpaRepository<TransferOperationLog, Long>{
    
}
