package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.Transfer;
import org.primefaces.model.SortOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TransferRepositoryCustom {


    public List<Transfer> retrieveTransfersByParam(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filterProps);

    public Long retrieveTransfersCountByParam(Map<String, Object> paramterMaps, Map<String, String> filterProps);
    
    public List<Transfer> getTransfersTimeBetween(Date startDate, Date endDate, int first, int pageSize, String sortField, SortOrder sortOrder);
    
    public Long getTransfersTimeBetweenCount(Date startDate, Date endDate);
    
    public List<Transfer> retrieveOpenOrders(int first, int pageSize, String sortField, SortOrder sortOrder);
    
    public Long retrieveOpenOrdersCount();

    public Long retrieveGoodsInDailyCount();

    public List<Transfer> retrieveGoodsInDailyTransfer(int first, int pageSize, String sortField, SortOrder sortOrder);

    public Long retrieveGoodsOutDailyTransferCount();

    public List<Transfer> retrieveGoodsOutDailyTransfer(int first, int pageSize, String sortField, SortOrder sortOrder);

    public List<Transfer> getGoodsInAcToStatus();

    public List<Transfer> getGoodsOutAcToStatus();

	public List<Transfer> retrieveGoodsOutDailyTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder);

	public Long retrieveGoodsOutDailyTransferCountForFilter(Map<String, Object> paramterMaps);

	public Long retrieveGoodsInDailyTransferCountForFilter(Map<String, Object> paramterMaps);

	public List<Transfer> retrieveGoodsInDailyTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder);

	public List<Transfer> retrieveGoodsInPastTransfer(int first, int pageSize, String sortField, SortOrder sortOrder,
			Date date);



	public Long retrieveGoodsInPastCount(Date date);

	
	public List<Transfer> retrieveGoodsInPastTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder, Date date);

	public Long retrieveGoodsInPastTransferCountForFilter(Map<String, Object> paramterMaps, Date date);

	public List<Transfer> getGoodsInAcToStatus2(Date date);

	public List<Transfer> getGoodsOutAcToStatus2(Date date);

	public List<Transfer> retrieveGoodsOutPastTransfer(int first, int pageSize, String sortField, SortOrder sortOrder,
			Date date);

	public Long retrieveGoodsOutPastTransferCount(Date date);

	public List<Transfer> retrieveGoodsOutPastTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder, Date date);

	public Long retrieveGoodsOutPastTransferCountForFilter(Map<String, Object> paramterMaps, Date date);
    

	public Long retrieveOpenOrdersByParamCount(Map<String, Object> paramMaps);

	public List<Transfer> retrieveOpenOrdersByParam(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> paramterMaps);

	public List<Transfer> retrieveOpenOrders();

	
	public List<Transfer> getTransfersTimeBetween2(Date startDate, Date endDate);

    public List<Transfer> retrieveTransfersStatus4(int first, int pageSize, String sortField, SortOrder sortOrder);

    public Long retrieveTransfersStatus4Count();

    public List<Transfer> retrieveStatus4ForFilter(Map<String, Object> paramterMaps, int first, int pageSize, String sortField,
                                                   SortOrder sortOrder);

    public Long retrieveTransferCountStatus4ForFilter(Map<String, Object> paramterMaps);

    List<Transfer> retrieveTransfersByParamforExcel(Map<String, Object> paramterMaps, Map<String, String> filterProps);

    List<Transfer> retrieveGoodsInPastTransferForFilterForExcel(Map<String, Object> paramterMaps, Date date);


    List<Transfer> retrieveGoodsOutPastTransferForFilterForExcel(Map<String, Object> paramterMaps, Date date);

    List<Transfer> retrieveOpenOrdersByParamForExcel(Map<String, Object> paramterMaps);


    
    
    
}
