/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.RobobackStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author ASUS
 */
public interface RobabackStatusRepo extends JpaRepository<RobobackStatus, Long> {

    @Query(value = "select * from  dbo.roboback_status where status_type = 'EKOL_STATUS';", nativeQuery = true)
    public List<RobobackStatus> getEkolStatus();

    @Query(value = "select * from  dbo.roboback_status where status_type = 'APP_STATUS';", nativeQuery = true)
    public List<RobobackStatus> getAppStatus();

}
