package com.setek.tr.centralwarehouse.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.setek.tr.centralwarehouse.model.UpsertTransferOrder;

@Repository
public interface UpsertTransferOrderRepository extends CrudRepository<UpsertTransferOrder, Long> {

	@Query(value = "SELECT * FROM dbo.upsert_transfer_order  where process_date >=?1 and process_date <=?2", nativeQuery = true)
	List<UpsertTransferOrder> getTodayLoadedUpserts(String processDate,String tomorrow);

}
