/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.setek.tr.centralwarehouse.model.RobobackSettings;

 
//@author turgut.simsek
@Repository
public interface RobobackSettingsRepo extends JpaRepository<RobobackSettings, Long> {
    
}
