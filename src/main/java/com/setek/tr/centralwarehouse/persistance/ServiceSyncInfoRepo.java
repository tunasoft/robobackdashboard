package com.setek.tr.centralwarehouse.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.setek.tr.centralwarehouse.model.ServiceSyncInfo;

@Repository
public interface ServiceSyncInfoRepo extends JpaRepository<ServiceSyncInfo, Long> {

	@Query(value = "select * from  dbo.service_sync_info where service_name >= ?1", nativeQuery = true)
	public ServiceSyncInfo getSyncInfoByServiceName(String serviceName);

}
