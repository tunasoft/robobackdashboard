package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.ArticleGroupName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleGroupNameRepository extends JpaRepository<ArticleGroupName, Integer> {
}
