/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ASUS
 */
@Repository
public interface TransferRepository extends JpaRepository<Transfer, Long> , TransferRepositoryCustom {

  /*  @Query(value = "SELECT * FROM "+ DataBaseConstant.Schemas.DBO+"."+ DataBaseConstant.Tables.TRANSFER +"WHERE OLUSTURMA_TARIHI >= ?1", nativeQuery = true)
    List<Transfer> findByCreationDateTransfers(String date);
*/

    List<Transfer> findByOlusturmaTarihi(Date date);

    List<Transfer> findAllByOlusturmaTarihiBetween(Date olusturmaTarihi, Date olusturmaTarihi2);

    /*  List<Transfer> findAllByExternalOrderNoIn(Set<Long> externalIds); */

    @Query(value = "SELECT * FROM dbo.transfer where external_order_no in (:params) ", nativeQuery = true)
    List<Transfer> getTransferByExternalOrderId(@Param("params") Set<String> externalOrderIds);


    List<Transfer> getTransferBySiparisNo(String orderNo);

    boolean existsBySiparisNo(String siparisNo);

   /* @Query(value = "SELECT * FROM roboback.dbo.transfer WHERE OLUSTURMA_TARIHI  between ?1 and ?2 order by siparis_no desc", nativeQuery = true)
    List<Transfer> findAllByCreatingTimeBetween(String createTimeStart , String createTimeEnd);



    @Query(value = "SELECT * FROM roboback.dbo.transfer WHERE SIPARIS_NO = ?1", nativeQuery = true)
    List<Transfer> findTransfersBySiparisNo(String siparisNo);
 */
}
