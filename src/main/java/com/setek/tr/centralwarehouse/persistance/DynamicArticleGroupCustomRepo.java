package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.DynamicArticleGroup;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

public interface DynamicArticleGroupCustomRepo {

    List<DynamicArticleGroup> retrieveArticelsByParams(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder);
}
