package com.setek.tr.centralwarehouse.persistance;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.setek.tr.centralwarehouse.model.StockRobo;

public interface StockRepositoryCustom {

	public List<StockRobo> retrieveStocksByParam(Map<String, Object> paramMaps, int first, int pageSize, String sortField, SortOrder sortOrder);

	public Long retrieveStocksCountByParam(Map<String, Object> paramMaps);
	
	public List<StockRobo>  getStocksTimeBetween(Date startDate , Date endDate ,int first, int pageSize, String sortField, SortOrder sortOrder);
	
	public Long getStockTimeBetweenCount(Date startDate , Date endDate);

	public List<StockRobo> getStocksTimeBetween(Date startDate, Date endDate);

	List<StockRobo> retrieveStocksByParamForExcel(Map<String, Object> paramMaps);
	
}
