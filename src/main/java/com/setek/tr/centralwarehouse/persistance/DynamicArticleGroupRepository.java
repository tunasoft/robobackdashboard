package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.DynamicArticleGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DynamicArticleGroupRepository extends JpaRepository<DynamicArticleGroup, Integer>, DynamicArticleGroupCustomRepo {
}
