package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.StockRobo;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class StockRepositoryImpl implements StockRepositoryCustom{


    @PersistenceContext
	private EntityManager em;
	private Logger log = LoggerFactory.getLogger(StockRepositoryImpl.class);


	@Override
	public List<StockRobo> retrieveStocksByParam(Map<String, Object> paramMaps, int first, int pageSize, String sortField, SortOrder sortOrder) {


        log.info("StockRepositoryImpl.retrieveStocksByParam()");

		Criteria criteria = em.unwrap(Session.class).createCriteria(StockRobo.class);

		for (String key : paramMaps.keySet()) {
			Object value = paramMaps.get(key);

//			criteria.add(Restrictions.sqlRestriction(" convert(varchar(4)," + key + ", 126 ) like '%" + value + "%'"));

			if (key.equals("ekol_Stock") || key.equals("dwh_Stock") || key.equals("control_Date")) {
				criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

			} else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}

		}

		if (sortField == null) {
			criteria.addOrder(Order.desc("controlDate"));
		} else if (sortOrder.equals(SortOrder.ASCENDING)) {
			criteria.addOrder(Order.asc(sortField));
		} else if (sortOrder.equals(SortOrder.DESCENDING)) {
			criteria.addOrder(Order.desc("controlDate"));
		}

		if (first >= 0) {
			criteria.setFirstResult(first);
		}
		if (pageSize >= 0) {
			criteria.setMaxResults(pageSize);
		}

		List<StockRobo> list = criteria.list();

		return list;


    }
	@Override
	public List<StockRobo> retrieveStocksByParamForExcel(Map<String, Object> paramMaps) {


        log.info("StockRepositoryImpl.retrieveStocksByParamForExcel()");

		Criteria criteria = em.unwrap(Session.class).createCriteria(StockRobo.class);

		for (String key : paramMaps.keySet()) {
            Object value = paramMaps.get(key);
            Integer val = 0;

            if (!key.equals("controlDate")) {
                val = Integer.parseInt(value.toString());
            }

            key = key.equals("ekolStock") ? "ekol_Stock" : key;
            key = key.equals("dwhStock") ? "dwh_Stock" : key;
            key = key.equals("controlDate") ? "control_Date" : key;



				if (key.equals("ekol_Stock") || key.equals("dwh_Stock") ) {
				criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

			} else if(key.equals("control_Date")) {
				criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value.toString().trim() + "%'"));

			}
				else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}
		}



		List<StockRobo> list = criteria.list();

		return list;


    }
	@Override
	public Long retrieveStocksCountByParam(Map<String, Object> paramMaps) {

		log.info("StockRepositoryImpl.retrieveStocksCountByParam()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(StockRobo.class);

		for (String key : paramMaps.keySet()) {
			Object value = paramMaps.get(key);

			if (key.equals("ekol_Stock") || key.equals("dwh_Stock") || key.equals("control_Date")) {
				criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

			} else {
				criteria.add(Restrictions.like(key, "%" + value + "%"));
			}

		}

		criteria.setProjection(Projections.rowCount());
		final Long result = (Long) criteria.uniqueResult();
		return result;
	}

	@Override
	public List<StockRobo>  getStocksTimeBetween(Date startDate , Date endDate ,int first, int pageSize, String sortField, SortOrder sortOrder){


        log.info("StockRepositoryImpl.getStocksTimeBetween()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(StockRobo.class);

        criteria.add(Restrictions.between("controlDate", startDate, endDate));


        if (sortField == null) {
			criteria.addOrder(Order.desc("id"));
		} else if (sortOrder.equals(SortOrder.ASCENDING)) {
			criteria.addOrder(Order.asc(sortField));
		} else if (sortOrder.equals(SortOrder.DESCENDING)) {
			criteria.addOrder(Order.desc("id"));
		}

		if (first >= 0) {
			criteria.setFirstResult(first);
		}
		if (pageSize >= 0) {
			criteria.setMaxResults(pageSize);
		}

        List<StockRobo> list = criteria.list();

		return list;

    }

	@Override
	public List<StockRobo>  getStocksTimeBetween(Date startDate , Date endDate ){


        log.info("StockRepositoryImpl.getStocksTimeBetween()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(StockRobo.class);

        criteria.add(Restrictions.between("controlDate", startDate, endDate));


        List<StockRobo> list = criteria.list();

		return list;

    }

    @Override
	public Long getStockTimeBetweenCount(Date startDate , Date endDate) {
	    log.info("StockRepositoryImpl.getStockTimeBetweenCount()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(StockRobo.class);

        criteria.add(Restrictions.between("controlDate", startDate, endDate));

		criteria.setProjection(Projections.rowCount());
		final Long result = (Long) criteria.uniqueResult();
		return result;
	}


}
