package com.setek.tr.centralwarehouse.persistance;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.setek.tr.centralwarehouse.model.ErrorLogs;

public interface ErrorLogsRepositoryCustom {
	public List<ErrorLogs> retrieveLogsByParam(Map<String, Object> paramterMaps, int first, int pageSize,
			String sortField, SortOrder sortOrder);
	
	public List<ErrorLogs> retrieveLogs(int first, int pageSize,
			String sortField, SortOrder sortOrder);
	
	public Long retrieveLogsCount();
	
	public Long retrieveLogsCountByParam(Map<String, Object> paramterMaps);
}
