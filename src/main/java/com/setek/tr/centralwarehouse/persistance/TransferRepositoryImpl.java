package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.Status;
import com.setek.tr.centralwarehouse.model.Transfer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.*;

public class TransferRepositoryImpl implements TransferRepositoryCustom {

    private Logger log = LoggerFactory.getLogger(TransferRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Transfer> retrieveTransfersByParam(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filterProps) {

        log.info("TransferRepositoryImpl.retrieveTransfersByParam()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" +  value + "'"));
            } else if ("entered_quantity_by_ekol".equals(key) ||  "quantity_transfer".equals(key) || key.equals("order_type_code") || key.equals("ekol_status") || key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("shipment_date") || key.equals("art_status") || key.equals("process_date")) {

                if (key.contains("date")) {
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }

                if (!filterProps.getOrDefault(key, "=").equals("like")) {
                    criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) " + filterProps.getOrDefault(key, "=") + value));
                } else {
                    criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

                }
            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public List<Transfer> retrieveTransfersByParamforExcel(Map<String, Object> paramterMaps, Map<String, String> filterProps) {

        log.info("TransferRepositoryImpl.retrieveTransfersByParamforExcel()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if ("entered_quantity_by_ekol".equals(key) || "quantity_transfer".equals(key) || key.equals("order_type_code") || key.equals("ekol_status") || key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if (key.contains("date")) {
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }

                if (!filterProps.getOrDefault(key, "=").equals("like")) {
                    criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) " + filterProps.getOrDefault(key, "=") + value));
                } else {
                    criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

                }
            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public Long retrieveTransfersCountByParam(Map<String, Object> paramterMaps, Map<String, String> filterProps) {

        log.info("TransferRepositoryImpl.retrieveTransfersCountByParam()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if ("entered_quantity_by_ekol".equals(key) || "quantity_transfer".equals(key) || key.equals("order_type_code") || key.equals("ekol_status") || key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("shipment_date") || key.equals("art_status") || key.equals("process_date")) {
                if (key.contains("date")) {
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                if (!filterProps.getOrDefault(key, "=").equals("like")) {
                    criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) " + filterProps.getOrDefault(key, "=") + value));
                } else {
                    criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));
                }

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> getTransfersTimeBetween(Date startDate, Date endDate, int first, int pageSize, String sortField, SortOrder sortOrder) {

        log.info("TransferRepositoryImpl.getTransfersTimeBetween()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        criteria.add(Restrictions.between("olusturmaTarihi", startDate, endDate));

        if (sortField == null) {
            criteria.addOrder(Order.desc("id"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("id"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public List<Transfer> getTransfersTimeBetween2(Date startDate, Date endDate) {

        log.info("TransferRepositoryImpl.getTransfersTimeBetween2()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        criteria.add(Restrictions.between("olusturmaTarihi", startDate, endDate));

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public Long getTransfersTimeBetweenCount(Date startDate, Date endDate) {

        log.info("TransferRepositoryImpl.getTransfersTimeBetweenCount()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        criteria.add(Restrictions.between("olusturmaTarihi", startDate, endDate));

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveOpenOrders(int first, int pageSize, String sortField, SortOrder sortOrder) {
        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        List<String> statusGoodsIn = new ArrayList<String>();
        statusGoodsIn.add("V");
        statusGoodsIn.add("O");
        statusGoodsIn.add("T");

        List<String> siparisTipiList = new ArrayList<String>();
        siparisTipiList.add("ORDER");
        siparisTipiList.add("TRANSFER ORDER");

        LogicalExpression rest1 = Restrictions.and(Restrictions.in("artDurum", statusGoodsIn), Restrictions.in("siparisTipi", siparisTipiList));

        LogicalExpression rest2 = Restrictions.and(Restrictions.eq("artDurum", "O"), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"));

        criteria.add(Restrictions.or(rest1, rest2));

        List<Status> status = new ArrayList<Status>();
        status.add(Status.New);
        status.add(Status.SentToEkol);
        status.add(Status.ProcessedbyEKOL);
        status.add(Status.ProcessedbyRobot);
        status.add(Status.ProcessedbyRobotFailed);

        criteria.add(Restrictions.in("status", status));

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveOpenOrders() {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        List<String> statusGoodsIn = new ArrayList<String>();
        statusGoodsIn.add("V");
        statusGoodsIn.add("O");
        statusGoodsIn.add("T");

        List<String> siparisTipiList = new ArrayList<String>();
        siparisTipiList.add("ORDER");
        siparisTipiList.add("TRANSFER ORDER");

        LogicalExpression rest1 = Restrictions.and(Restrictions.in("artDurum", statusGoodsIn), Restrictions.in("siparisTipi", siparisTipiList));

        LogicalExpression rest2 = Restrictions.and(Restrictions.eq("artDurum", "O"), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"));

        criteria.add(Restrictions.or(rest1, rest2));

        List<Status> status = new ArrayList<Status>();
        status.add(Status.New);
        status.add(Status.SentToEkol);
        status.add(Status.ProcessedbyEKOL);
        status.add(Status.ProcessedbyRobot);
        status.add(Status.ProcessedbyRobotFailed);

        criteria.add(Restrictions.in("status", status));

        List<Transfer> list = criteria.list();

        return list;

    }

    @Override
    public Long retrieveOpenOrdersCount() {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        List<String> statusGoodsIn = new ArrayList<String>();
        statusGoodsIn.add("V");
        statusGoodsIn.add("O");
        statusGoodsIn.add("T");

        List<String> siparisTipiList = new ArrayList<String>();
        siparisTipiList.add("ORDER");
        siparisTipiList.add("TRANSFER ORDER");

        LogicalExpression rest1 = Restrictions.and(Restrictions.in("artDurum", statusGoodsIn), Restrictions.in("siparisTipi", siparisTipiList));

        LogicalExpression rest2 = Restrictions.and(Restrictions.eq("artDurum", "O"), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"));

        criteria.add(Restrictions.or(rest1, rest2));

        List<Status> status = new ArrayList<Status>();
        status.add(Status.New);
        status.add(Status.SentToEkol);
        status.add(Status.ProcessedbyEKOL);
        status.add(Status.ProcessedbyRobot);
        status.add(Status.ProcessedbyRobotFailed);

        criteria.add(Restrictions.in("status", status));

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveOpenOrdersByParam(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> paramterMaps) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        List<String> statusGoodsIn = new ArrayList<String>();
        statusGoodsIn.add("V");
        statusGoodsIn.add("O");
        statusGoodsIn.add("T");

        List<String> siparisTipiList = new ArrayList<String>();
        siparisTipiList.add("ORDER");
        siparisTipiList.add("TRANSFER ORDER");

        LogicalExpression rest1 = Restrictions.and(Restrictions.in("artDurum", statusGoodsIn), Restrictions.in("siparisTipi", siparisTipiList));

        LogicalExpression rest2 = Restrictions.and(Restrictions.eq("artDurum", "O"), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"));

        criteria.add(Restrictions.or(rest1, rest2));

        List<Status> status = new ArrayList<Status>();
        status.add(Status.New);
        status.add(Status.SentToEkol);
        status.add(Status.ProcessedbyEKOL);
        status.add(Status.ProcessedbyRobot);
        status.add(Status.ProcessedbyRobotFailed);

        criteria.add(Restrictions.in("status", status));
        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("status")
                    || key.equals("art_status")) {

                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveOpenOrdersByParamForExcel(Map<String, Object> paramterMaps) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        List<String> statusGoodsIn = new ArrayList<String>();
        statusGoodsIn.add("V");
        statusGoodsIn.add("O");
        statusGoodsIn.add("T");

        List<String> siparisTipiList = new ArrayList<String>();
        siparisTipiList.add("ORDER");
        siparisTipiList.add("TRANSFER ORDER");

        LogicalExpression rest1 = Restrictions.and(Restrictions.in("artDurum", statusGoodsIn), Restrictions.in("siparisTipi", siparisTipiList));

        LogicalExpression rest2 = Restrictions.and(Restrictions.eq("artDurum", "O"), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"));

        criteria.add(Restrictions.or(rest1, rest2));

        List<Status> status = new ArrayList<Status>();
        status.add(Status.New);
        status.add(Status.SentToEkol);
        status.add(Status.ProcessedbyEKOL);
        status.add(Status.ProcessedbyRobot);
        status.add(Status.ProcessedbyRobotFailed);

        criteria.add(Restrictions.in("status", status));

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            key = key.equals("orderTypeCode") ? "order_type_code" : key;
            key = key.equals("ekolStatus") ? "ekol_status" : key;
            key = key.equals("artNo") ? "art_no" : key;
            key = key.equals("siparisAdet") ? "order_quantity" : key;
            key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
            key = key.equals("olusturmaTarihi") ? "create_date" : key;
            key = key.equals("sevkTarihi") ? "shipment_date" : key;
            key = key.equals("artDurum") ? "art_status" : key;

            if (key.equals("status")) {
                value = Status.valueOf(value.toString()).getDescription();

            }

            if (key.equals("order_type_code") || key.equals("ekol_status") || key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("status")
                    || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        List<Transfer> list = criteria.list();

        return list;
    }
    
    @Transactional(readOnly = true)
    @Override
    public Long retrieveOpenOrdersByParamCount(Map<String, Object> paramMaps) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        List<String> statusGoodsIn = new ArrayList<String>();
        statusGoodsIn.add("V");
        statusGoodsIn.add("O");
        statusGoodsIn.add("T");

        List<String> siparisTipiList = new ArrayList<String>();
        siparisTipiList.add("ORDER");
        siparisTipiList.add("TRANSFER ORDER");

        LogicalExpression rest1 = Restrictions.and(Restrictions.in("artDurum", statusGoodsIn), Restrictions.in("siparisTipi", siparisTipiList));

        LogicalExpression rest2 = Restrictions.and(Restrictions.eq("artDurum", "O"), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"));

        criteria.add(Restrictions.or(rest1, rest2));

        List<Status> status = new ArrayList<Status>();
        status.add(Status.New);
        status.add(Status.SentToEkol);
        status.add(Status.ProcessedbyEKOL);
        status.add(Status.ProcessedbyRobot);
        status.add(Status.ProcessedbyRobotFailed);

        criteria.add(Restrictions.in("status", status));

        for (String key : paramMaps.keySet()) {
            Object value = paramMaps.get(key);

            if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date")|| key.equals("process_date") || key.equals("shipment_date") || key.equals("status")
                    || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    @Override
    public List<Transfer> getGoodsOutAcToStatus() {
        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        List<Transfer> t = criteria.add(Restrictions.and(
                Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date()))).list();
        return t;

    }

    @Override
    public List<Transfer> getGoodsInAcToStatus() {
        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};
        List<Transfer> t = criteria
                .add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date()))).list();
        return t;

    }

    @Override
    public List<Transfer> getGoodsOutAcToStatus2(Date date) {
        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        List<Transfer> t = criteria.add(Restrictions.and(
                Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate))).list();
        return t;

    }

    @Override
    public List<Transfer> getGoodsInAcToStatus2(Date date) {
        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};
        List<Transfer> t = criteria
                .add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)))
                .list();
        return t;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveGoodsOutDailyTransfer(int first, int pageSize, String sortField, SortOrder sortOrder) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date())));

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        List<Transfer> t = criteria.list();
        return t;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveGoodsOutPastTransfer(int first, int pageSize, String sortField, SortOrder sortOrder, Date date) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);

        Status[] statusNames = {Status.ProcessedbyRobotSuccesfully, Status.ProcessedbyRobot};
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        List<Transfer> t = criteria.list();
        return t;
    }

    @Override
    public Long retrieveGoodsOutDailyTransferCount() {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date()))).list();

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public Long retrieveGoodsOutPastTransferCount(Date date) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);
        
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)))
                .list();

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveGoodsInDailyTransfer(int first, int pageSize, String sortField, SortOrder sortOrder) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date())));

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        List<Transfer> t = criteria.list();
        return t;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveGoodsInPastTransfer(int first, int pageSize, String sortField, SortOrder sortOrder, Date date) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);

        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        List<Transfer> t = criteria.list();
        return t;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveTransfersStatus4(int first, int pageSize, String sortField, SortOrder sortOrder) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Status[] statusNames = {Status.valueOf("ProcessedbyRobotFailed")};
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames))).list();

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }
        List<Transfer> t = criteria.list();
        return t;
    }

    @Override
    public Long retrieveTransfersStatus4Count() {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Status[] statusNames = {Status.valueOf("ProcessedbyRobotFailed")};

        criteria.add(Restrictions.and(Restrictions.in("status", statusNames))).list();

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveStatus4ForFilter(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder) {

        Status[] statusNames = {Status.valueOf("ProcessedbyRobotFailed")};

        log.info("TransferRepositoryImpl.retrieveStatus4ForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames))).list();

        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public Long retrieveTransferCountStatus4ForFilter(Map<String, Object> paramterMaps) {

        Status[] statusNames = {Status.valueOf("ProcessedbyRobotFailed")};
        log.info("TransferRepositoryImpl.retrieveTransferCountStatus4ForFilters()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }

        criteria.add(Restrictions.and(Restrictions.in("status", statusNames))).list();

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public Long retrieveGoodsInPastCount(Date date) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)))
                .list();

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public Long retrieveGoodsInDailyCount() {

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date()))).list();

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveGoodsOutDailyTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder) {
        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsOutDailyTransferForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date())));
        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Transfer> retrieveGoodsOutPastTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder, Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsOutPastTransferForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));
        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public List<Transfer> retrieveGoodsOutPastTransferForFilterForExcel(Map<String, Object> paramterMaps, Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsOutPastTransferForFilterForExcel()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            key = key.equals("processDate") ? "process_date" : key;
            key = key.equals("artNo") ? "art_no" : key;
            key = key.equals("siparisAdet") ? "order_quantity" : key;
            key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
            key = key.equals("olusturmaTarihi") ? "create_date" : key;
            key = key.equals("sevkTarihi") ? "shipment_date" : key;
            key = key.equals("artDurum") ? "art_status" : key;

            if (key.equals("status")) {
                value = Status.valueOf(value.toString()).getDescription();

            }

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public Long retrieveGoodsOutDailyTransferCountForFilter(Map<String, Object> paramterMaps) {
        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsOutDailyTransferCountForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));
            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date())));
        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public Long retrieveGoodsOutPastTransferCountForFilter(Map<String, Object> paramterMaps, Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsOutPastTransferCountForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));

            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.eq("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));
        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public List<Transfer> retrieveGoodsInDailyTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder) {
        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsInDailyTransferForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));

            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date())));
        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public List<Transfer> retrieveGoodsInPastTransferForFilter(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder, Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsInPastTransferForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));

            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));
        if (sortField == null) {
            criteria.addOrder(Order.desc("siparisNo"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc("siparisNo"));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public List<Transfer> retrieveGoodsInPastTransferForFilterForExcel(Map<String, Object> paramterMaps, Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(00);
        requestedDate.setMinutes(00);
        requestedDate.setSeconds(00);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(00);
        oneDayBefore.setMinutes(00);
        oneDayBefore.setSeconds(00);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsInPastTransferForFilterForExcel()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            key = key.equals("artNo") ? "art_no" : key;
            key = key.equals("siparisAdet") ? "order_quantity" : key;
            key = key.equals("siparisGirilen") ? "entered_quantity_by_store" : key;
            key = key.equals("olusturmaTarihi") ? "create_date" : key;
            key = key.equals("sevkTarihi") ? "shipment_date" : key;
            key = key.equals("artDurum") ? "art_status" : key;

            if (key.equals("status")) {
                value = Status.valueOf(value.toString()).getDescription();

            }

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));

            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));

            }
        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));

        List<Transfer> list = criteria.list();

        return list;
    }

    @Override
    public Long retrieveGoodsInDailyTransferCountForFilter(Map<String, Object> paramterMaps) {
        Date yesterday = yesterday();
        yesterday.setHours(23);
        yesterday.setMinutes(59);
        yesterday.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveTransfersCountByParam()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));

            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", yesterday, new Date())));
        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public Long retrieveGoodsInPastTransferCountForFilter(Map<String, Object> paramterMaps, Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        Date requestedDate = cal.getTime();
        requestedDate.setHours(23);
        requestedDate.setMinutes(59);
        requestedDate.setSeconds(59);
        cal.add(Calendar.DATE, -1);

        Date oneDayBefore = cal.getTime();
        oneDayBefore.setHours(23);
        oneDayBefore.setMinutes(59);
        oneDayBefore.setSeconds(59);
        Status[] statusNames = {Status.valueOf("ProcessedbyRobotSuccesfully"), Status.valueOf("ProcessedbyRobot")};

        log.info("TransferRepositoryImpl.retrieveGoodsInPastTransferCountForFilter()");

        Criteria criteria = em.unwrap(Session.class).createCriteria(Transfer.class);

        for (String key : paramterMaps.keySet()) {
            Object value = paramterMaps.get(key);

            if (key.equals("status")) {

                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) = '" + value + "'"));

            } else if (key.equals("art_no") || key.equals("order_quantity") || key.equals("entered_quantity_by_store") || key.equals("create_date") || key.equals("process_date") || key.equals("shipment_date") || key.equals("art_status")) {
                if(key.contains("date")){
                    value = new SimpleDateFormat("yyyy-MM-dd").format((Date) value);
                }
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }
        criteria.add(Restrictions.and(Restrictions.in("status", statusNames), Restrictions.ne("siparisTipi", "OUTGOING TRANSFER"), Restrictions.between("processDate", oneDayBefore, requestedDate)));
        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

}
