package com.setek.tr.centralwarehouse.persistance;

import com.setek.tr.centralwarehouse.model.DynamicArticleGroup;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;

public class DynamicArticleGroupCustomRepoImpl implements DynamicArticleGroupCustomRepo {


    private Logger log = LoggerFactory.getLogger(DynamicArticleGroupCustomRepoImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DynamicArticleGroup> retrieveArticelsByParams(Map<String, Object> paramterMaps, int first, int pageSize, String sortField, SortOrder sortOrder) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(DynamicArticleGroup.class);

        for (String parameter : paramterMaps.keySet()) {

            Object value = paramterMaps.get(parameter);

            //TODO: if bloğunu denemek için yaptım düzenleyeceğim
                criteria.add(Restrictions.sqlRestriction("cast(" + parameter + " as char) like '%" + value + "%'"));
        }

        if (sortField == null) {
            criteria.addOrder(Order.desc("group_code"));
        } else if (sortOrder.equals(SortOrder.ASCENDING)) {
            criteria.addOrder(Order.asc(sortField));
        } else if (sortOrder.equals(SortOrder.DESCENDING)) {
            criteria.addOrder(Order.desc(sortField));
        }

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<DynamicArticleGroup> dynamicArticleGroups = criteria.list();


        return dynamicArticleGroups;
    }
}
