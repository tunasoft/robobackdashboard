package com.setek.tr.centralwarehouse.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.setek.tr.centralwarehouse.model.ErrorLogs;

public interface ErrorLogsRepository   extends JpaRepository<ErrorLogs, Long>, ErrorLogsRepositoryCustom{

}
