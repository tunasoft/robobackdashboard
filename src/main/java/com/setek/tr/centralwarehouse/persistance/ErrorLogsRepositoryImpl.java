package com.setek.tr.centralwarehouse.persistance;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;

import com.setek.tr.centralwarehouse.model.ErrorLogs;
import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Transactional;

public class ErrorLogsRepositoryImpl implements ErrorLogsRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Transactional(readOnly = true)
    @Override
    public List<ErrorLogs> retrieveLogsByParam(Map<String, Object> paramterMaps, int first, int pageSize,
            String sortField, SortOrder sortOrder) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(ErrorLogs.class);

        for (String key : paramterMaps.keySet()) {

            Object value = paramterMaps.get(key);

            if (key.equals("service_name") || key.equals("error_date") || key.equals("summary_error")
                    || key.equals("error")) {
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + "%"));
            }

        }
        criteria.addOrder(Order.desc("id"));

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<ErrorLogs> list = criteria.list();

        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ErrorLogs> retrieveLogs(int first, int pageSize,
            String sortField, SortOrder sortOrder) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(ErrorLogs.class);

        criteria.addOrder(Order.desc("id"));

        if (first >= 0) {
            criteria.setFirstResult(first);
        }
        if (pageSize >= 0) {
            criteria.setMaxResults(pageSize);
        }

        List<ErrorLogs> list = criteria.list();

        return list;
    }

    @Override
    public Long retrieveLogsCount() {
        Criteria criteria = em.unwrap(Session.class).createCriteria(ErrorLogs.class);
        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }

    @Override
    public Long retrieveLogsCountByParam(Map<String, Object> paramterMaps) {

        Criteria criteria = em.unwrap(Session.class).createCriteria(ErrorLogs.class);

        for (String key : paramterMaps.keySet()) {

            Object value = paramterMaps.get(key);

            if (key.equals("service_name") || key.equals("error_date") || key.equals("summary_error")
                    || key.equals("error")) {
                criteria.add(Restrictions.sqlRestriction("cast(" + key + " as char) like '%" + value + "%'"));

            } else {
                criteria.add(Restrictions.like(key, "%" + value + ""
                        + "+%"));
            }

        }

        criteria.setProjection(Projections.rowCount());
        final Long result = (Long) criteria.uniqueResult();
        return result;

    }
}
